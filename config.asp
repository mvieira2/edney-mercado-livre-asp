<!--#include file = "aspJSON1.17.asp"-->

<%
Const API_URL = "https://api.mercadolibre.com"
Const APP_ID  = "1535946541127628"
Const SECRET_KEY = "eqvypRk4vxwqLoU2JMWGRpLVuqcO7iR8"
Const REDIRECT_URI = "http://localhost/edney-mercado-livre-asp/gerarToken.asp"
Const REFRESH_TOKEN = ""

Const DATABASE_URL = "177.70.99.247"
Const DATABASE = "OminicDB"
Const UID = "sql_ml"
Const PWD = "2019@ml"


'=======================================================================

Dim SERVER_GENERATED_AUTHORIZATION_CODE, ML_Auth_url, ML_access_token_url, resolveTimeout, conectaTimeout, sendTimeout, receiveTimeout, oHttpRequest, jsonString, teste, ML_Refresh_Token

ML_Auth_url = "https://auth.mercadolibre.com.ar/authorization?response_type=code&client_id=" & APP_ID
ML_access_token_url  = "/oauth/token?grant_type=authorization_code&client_id="& APP_ID &"&client_secret="& SECRET_KEY &"&code={SERVER_GENERATED_AUTHORIZATION_CODE}&redirect_uri="&REDIRECT_URI&" "
ML_refresh_token_url = "/oauth/token?grant_type=refresh_token&client_id="&APP_ID&"&client_secret="&SECRET_KEY&"&refresh_token={REFRESH_TOKEN}"


'=======================================================================

Function ML_logar
	Response.Redirect ML_Auth_url
	'response.write "<a href='"&ML_Auth_url&"'>Necessario Autenticar no Mercado Livre</a>"
end Function

'=======================================================================
Function ML_gerarToken

	Dim ML_url, retorno, redirect_last_url_saved

	If(Request.QueryString("code") <> "") then 'url para novo token
		ML_url = Replace(ML_access_token_url, "{SERVER_GENERATED_AUTHORIZATION_CODE}", Request.QueryString("code") )

	Else
		Call ML_logar
	End if

	dd(ML_url) & "<br>"
	retorno =  API(ML_url, "POST", "")

	dd retorno

	If(retorno <> False) then
		Call WriteFile("token.txt",  retorno ) ' Salvando o token
	Else
		dd " Erro ao recuperar um novo token "
	End If



	redirect_last_url_saved = Session.Contents("redirect_last_url_save")

	If ( redirect_last_url_saved <> "" ) then
		Delay(2)
		Session.Contents.Remove("redirect_last_url_save")
		Response.Redirect redirect_last_url_saved
	End If


end Function

'=======================================================================
Function ML_refreshToken

	Dim ML_url, retorno, refresh_token, redirect_last_url_saved

	refresh_token =  ML_getToken("refresh_token")

	If( refresh_token <> false and  refresh_token <> "" ) then 'url para refresh token
		ML_url = Replace(ML_refresh_token_url,"{REFRESH_TOKEN}", refresh_token )

	Else
		Call ML_logar
	End if



	dd(ML_url) & "<br>"
	retorno =  API(ML_url, "POST", "")

	dd retorno

	If(retorno <> False) then
		Call WriteFile("token.txt",  retorno ) ' Salvando o token
	Else
		dd " Erro ao atualizar token "
	End If

	redirect_last_url_saved = Session.Contents("redirect_last_url_save")

	If ( redirect_last_url_saved <> "" ) then
		Delay(2)
		Session.Contents.Remove("redirect_last_url_save")
		Response.Redirect redirect_last_url_saved
	End If



end Function

'=========================================================================
Function ML_getToken(campo)
	Dim JSON, jsonString

	jsonString = ReadFile("token.txt")

	If jsonString <> false then

		Set JSON = New aspJSON
		JSON.loadJSON(replace(jsonString,".","#"))  

		If( campo <> "") then

			ML_getToken = JSON.data(campo)
			Exit Function
		End If

		ML_getToken = JSON.data("access_token")

	Else 'Caso nao tenha 1° token
		Session("redirect_last_url_save") = Request.ServerVariables("SCRIPT_NAME")
		ML_gerarToken
	End If
End Function

'=========================================================================
Function ML_validarToken
	Session("redirect_last_url_save") = Request.ServerVariables("SCRIPT_NAME")
	ML_refreshToken
End Function

'=========================================================================
Function API(url, method, CorpoMesagem)

	resolveTimeout = 300
	conectaTimeout = 300
	sendTimeout = 300
	receiveTimeout = 300


	If( InStr(url,"{ACCESS_TOKEN}") ) then
		url = Replace( url, "{ACCESS_TOKEN}", ML_getToken("")  )
	End if


	If( InStr(url,"{USER_ID}") ) then
		url = Replace( url, "{USER_ID}", ML_getToken("user_id")  )
	End if


	If( CorpoMesagem = "" or CorpoMesagem = "null" ) then
		CorpoMesagem = ""
	end If

	Set oHttpRequest = CreateObject("MSXML2.ServerXMLHTTP")
	Dim JSONErros, JSONStringErros
	'response.ContentType = "application/json"

	with oHttpRequest 
		'.SetTimeouts resolveTimeout,conectaTimeout,sendTimeout,receiveTimeout
		.Open method, API_URL & url
		.SetRequestHeader "Accept",   "application/json"
		.SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
		.Send CorpoMesagem

		Select case .Status
			case 200 'ok
				API = .responseText
			Exit Function
			case 400 'Erros 
				
				Set JSONErros = New aspJSON
				JSONErros.loadJSON( .responseText )  
				JSONStringErros = JSONErros.data("message")

				If( InStr(JSONStringErros, "It has expired or it has already been used") > 0 ) then
					ML_validarToken
				End If

				dd(.responseText)
				API = false ' " nao foi encontrado."
			Exit Function	
			case 401 'Pagina nao localizada
				Session("redirect_last_url_save") = Request.ServerVariables("SCRIPT_NAME")
				ML_gerarToken
				API = false ' " nao foi encontrado."
			Exit Function
			case 404 'Pagina nao localizada
				dd(.responseText)
				API = false ' " nao foi encontrado."
			Exit Function
			case Else
				dd(.responseText)
				API = false ' "<pre>Ocorreu um erro nao previsto de HTTP : " & .Status  & "<br>" & .responseText & "</pre>"
			Exit Function
		End Select 
	End With 

    Set oHttpRequest = Nothing

end Function

'=========================================================================
Sub Delay(NumberOfSeconds)
	Dim DateTimeResume
	DateTimeResume= DateAdd("s", NumberOfSeconds, Now())
	Do Until (Now() > DateTimeResume)
	Loop
End Sub

'==========================================================================

Function WriteFile(path, texto)
	dim fs,fname
	set fs=Server.CreateObject("Scripting.FileSystemObject")
	set fname=fs.CreateTextFile(Server.MapPath(path),true)
	fname.WriteLine(texto)
	fname.Close
	set fname=nothing
	set fs=nothing
End Function

'==========================================================================
Function ReadFile(path)
	Set fs=Server.CreateObject("Scripting.FileSystemObject")

	If fs.FileExists( Server.MapPath(path) ) then
		Set f=fs.OpenTextFile(Server.MapPath(path), 1)
		ReadFile = f.ReadAll
		f.Close
		Set f=Nothing

	Else 
		ReadFile = false

	End If
	



	Set fs=Nothing
End Function

'==========================================================================


Function query(queyString)
    On Error Resume next

    ''//Create the ADO objects
    Dim rs, cmd
    Set rs = server.createobject("ADODB.Recordset")
    Set cmd = server.createobject("ADODB.Command")

    cmd.ActiveConnection =  "Provider=SQLOLEDB.1;Data Source=" & server_url & ";Initial Catalog=" & database & ";user id = '" & uid & "';password='" & pwd & "'"
    cmd.CommandText = queyString
    cmd.CommandTimeout = 900 


    rs.Open cmd
    If err.number > 0 then
       response.write err
        exit function
    end if

    ''// Disconnect the recordset
    Set cmd.ActiveConnection = Nothing
    Set cmd = Nothing
    Set rs.ActiveConnection = Nothing

    ''// Return the resultant recordset
    Set query = rs

End Function


'==========================================================================

Function dd(v)
	'response.write "<pre>" & v & "</pre> "
	response.write v
End Function
'==========================================================================

function ML_deParaStatus(prlabelS)

    if(prlabelS = "" or prlabelS = "null") then 
        ML_deParaStatus = "1"
        exit function
    end if

    select case trim(ucase(prlabelS))
        case "CONFIRMED" 'Status inicial de uma ordem; ainda sem ter sido paga.'
            ML_deParaStatus = "1" 
            exit function
        case "PAYMENT_REQUIRED" 'O pagamento da ordem deve ter sido confirmado para exibir as informações do usuário.'
            ML_deParaStatus = "2"
            exit function
        case "PAYMENT_IN_PROCESS" ' Há um pagamento relacionado à ordem, mais ainda não foi creditado.'
            ML_deParaStatus = "4"
            exit function
        case "PARTIALLY_PAID" 'A ordem tem um pagamento associado creditado, porém, insuficiente.'
            ML_deParaStatus = "56"
            exit function
        case "PAID" 'A ordem tem um pagamento associado creditado.'
            ML_deParaStatus = "336" 
            exit function
        case "CANCELLED" 'Por alguma razão, a ordem não foi completada.*'
            ML_deParaStatus = "7"
            exit function
        case "INVALID" 'A ordem foi invalidada por vir de um comprador malicioso.'
            ML_deParaStatus = "8"
            exit function
    end select 
end function 

%>