<!--#include file = "config.asp"-->
<%

  ' Robô – Puxar produtos do ML;
  ' o  Gravar pedido no Banco de dados;

  
  Dim ML_Orders_Url, resultPedidos, sqlSavePedido, sSqlInsertCompradorId
    CdclienteByToken = ML_getToken("user_id")

  response.ContentType = "application/json"


  'status confirmed, payment_required, payment_in_process, partially_paid, paid, cancelled, invalid, 
  'ML_Orders_Url = "/orders/search?seller={USER_ID}&order.status=paid&access_token={ACCESS_TOKEN}"
  ML_Orders_Url = "/orders/search?seller={USER_ID}&access_token={ACCESS_TOKEN}"

  'resultPedidos =  API( ML_Orders_Url, "GET", "")
  resultPedidos = ReadFile("json_pedidos.json")

  dd "[" & resultPedidos & "]"



 
If(resultPedidos <> "") then

    Set oJSON = New aspJSON
    oJSON.loadJSON(resultPedidos)


      For Each arr In oJSON.data("results")

        Set this = oJSON.data("results").item(arr)

            vat_numberC = this.item("buyer").item("billing_info").item("doc_number")

            Cdcliente = CdclienteByToken
            Id_Tipo = ""

            Id = this.item("id")
            Trachannel = ""
            Entrega_Diferente = ""
            neighborhoodS = ""
            detailS = ""
            TipoEntregaDescricao = ""
            TipoEntregaTransportadora = ""
            Code = ""
            IdOrderMarketplace = ""
            shippingCode = ""
            sRetorno = ""


            Id_Status = ML_deParaStatus( this.item("status") )
            TotalAmount = this.item("total_amount")
            TotalFreight = this.item("shipping").item("cost")
            streetS = this.item("shipping").item("receiver_address").item("address_line") 'com numero da residencia'
            cityS = this.item("shipping").item("receiver_address").item("city").item("name")
            sEstado = this.item("shipping").item("receiver_address").item("state").item("name") 'ou id'
            Id_FrmPagamento = this.item("payments").item(0).item("payment_method_id") 'array de modos de pagamentos'
            postcodeS = this.item("shipping").item("receiver_address").item("zip_code")
            numberS = this.item("shipping").item("receiver_address").item("street_number")
            
            nameC = this.item("buyer").item("first_name") & " " & this.item("buyer").item("first_name")
            emailC = this.item("buyer").item("email")
            phones1C = this.item("buyer").item("phone").item("area_code") & " " & this.item("buyer").item("phone").item("number")
            phones2C = this.item("buyer").item("alternative_phone").item("area_code") & " " & this.item("buyer").item("phone").item("number")

            'dd vat_numberC
            'dd "<br>"
            'dd query( "SELECT TOP 1 ID from TB_CLIENTES (NOLOCK) WHERE cpf = '"& vat_numberC &"' and cdcliente = " & CdclienteByToken )

            'dd DB_buyerId

            if(DB_buyerId) then

                sIdProdCad = DB_buyerId


            else


                sSqlInsertComprador = " INSERT INTO TB_CLIENTES (Cdcliente, Id_Tipo, Id_Provincia, Nome, Endereco, CEP, Numero, Cidade, Bairro, Complemento, Email, Senha, CPF, TipoPessoa, Telefone, Pais, TipoEndereco, Celular, Ativo) " & _ 
                                        " VALUES ('"& Cdcliente &"','"& Id_Tipo &"', '"& sEstado &"', '"& Replace(nameC,"'","") &"','"& streetS &"', '"& postcodeS &"', '"& numberS &"', '"& cityS &"', '"& neighborhoodS &"', '"& detailS &"', '"& emailC &"','123456','"& vat_numberC &"','F','"& phones1C &"','BR', 'R', '"& phones2C &"', 1) "

                'set sSqlInsertCompradorId = query(sSqlInsertComprador)

            end if




      

      sqlSavePedido	=	"INSERT INTO Tb_Pedidos ( " & _
          "CdCliente,	" & _
            "Id_Status,	" & _
            "DataCad,	" & _
            "VeioDoBanner,	" & _
            "Id_Cliente,	" & _
            "Id_Provincia,	" & _
            "Id_FrmPagamento,	" & _
            "Entrega_Diferente,	" & _
            "Endereco,	" & _
            "Cep,	" & _
            "Cidade,	" & _
            "Bairro,	" & _
            "Complemento,	" & _
            "Numero,	" & _
            "Pais,	" & _
            "SubTotal,	" & _
            "Total,	" & _
            "ValorParcela," & _
            "FreteReal,	" & _
            "Frete,	" & _
            "Parcelamento,	" & _
            "TipoEntregaDescricao,	" & _
            "TipoEntregaTransportadora,	" & _
            "CodigoServicoIG,	" & _
            "RetornoIntegracao2,	" & _
            "RetornoIntegracao3,	" & _
            "RetornoIntegracao5,	" & _
            "RetornoBcash	" & _   
          ") VALUES (" & _
            Cdcliente &"," & _
            " "& Id_Status &"," & _
            " getdate(),                                  " & _
            "'" & Trachannel					& "',   " & _
            "'" & sIdProdCad   				& "',   " & _
            "'" & sEstado           			& "',   " & _
            "'"& Id_FrmPagamento               &"',    " & _
            "'"& Entrega_Diferente             &"',    " & _
            "'" & streetS       				& "',   " & _
            "'" & postcodeS      				& "',   " & _
            "'" & cityS   					    & "',   " & _
            "'" & neighborhoodS   				& "',   " & _
            "'" & detailS   				    & "',   " & _
            "'" & numberS   					& "',   " & _
            "'BR',                                      " & _
            "'" &   Replace(Replace((cdbl(TotalAmount) - cdbl(TotalFreight)),".",""),",",".") &"',  " & _
            "'" &   Replace(Replace(TotalAmount,".",""),",",".") &"', " & _
            "'" &   Replace(Replace(TotalAmount,".",""),",",".") &"', " & _
            "'" &   Replace(Replace(TotalFreight,".",""),",",".") &"', " & _
            "'" &   Replace(Replace(TotalFreight,".",""),",",".") &"', " & _
            "1 ,                                        " & _
            "'"& TipoEntregaDescricao       &"',                   " & _
            "'"& TipoEntregaTransportadora  &"',                   " & _
            "'"& Code &"',                              " & _
            "'"& IdOrderMarketplace &"',                " & _
            "'"& Id &"',                                " & _
            "'"& shippingCode &"',          " & _
            "'"& replace(sRetorno, "'", "") &"' " & _
      ")"


      '' dd query(sqlSavePedido)


  Next

end If



%>