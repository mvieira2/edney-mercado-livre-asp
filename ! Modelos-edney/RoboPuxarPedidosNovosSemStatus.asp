﻿<!--#include file = "../../hidden/funcoes.asp"-->
<!--#include file = "../../hidden/aspJSON1.17/aspJSON1.17.asp"-->
<%
Server.ScriptTimeOut = 65000
Response.Buffer      = true

Response.Charset = "UTF-8" '---'***verificar fnc URLDecode2***

dim Rs, Rs1, sSql
dim sNrPedido, sNumeroObj, sEmailCliente, sCdClientePedido, sNomeCliente, sTipoServico, sIdCateg
dim sImei, sChip, sEstoqueTravado, ArrayItens(10, 20), sChaveWM, IdSellerNetShoes
dim LoginERP, SenhaERP, Chave, sIdMoedda, BoolPedidoCancelado, BoolPedidoErroAcento, UrlApiProduto2
dim totalJson, pageJson, totalPagesJson, sRetornoStatus

Chave                = Replace_Caracteres_Especiais(Request("chave"))

if(Chave = "") then 
    response.write "[ERRO(1)]"
    response.End
end if

On Error Resume Next

call Abre_Conexao()   

LoginERP            = trim(Recupera_Campos_Db_oConn("TBCLIENTE", Chave, "CdCliente", "LoginNetShoes"))
SenhaERP            = trim(Recupera_Campos_Db_oConn("TBCLIENTE", Chave, "CdCliente", "SenhaNetShoes")) 
sIdMoedda           = trim(Recupera_Campos_Db_oConn("TB_MOEDAS", Chave, "CdCliente", "id"))
IdSellerNetShoes    = trim(Recupera_Campos_Db_oConn("TBCLIENTE", Chave, "CdCliente", "IdSellerNetShoes"))
sRetorno            = Retornar_Json_MKT("1")


for lContPaginas = 1 to pageJson
   
    if(sRetorno <> "" and sRetornoStatus <> "404") then 

        Set oJSONLista = New aspJSON
        oJSONLista.loadJSON(replace(sRetorno,".","#"))
    
        totalJson       = oJSONLista.data("total")

        if(totalJson > 0 ) then    

            For Each PedidoQ In oJSONLista.data("items")

                Set CampoMainQ      = oJSONLista.data("items").item(PedidoQ)

                Id                  = CampoMainQ.item("originNumber")
                IdOrder             = CampoMainQ.item("orderNumber")
                IdOrderMarketplace  = CampoMainQ.item("orderNumber")
                code                = CampoMainQ.item("orderNumber")
                InsertedDate        = CampoMainQ.item("orderDate")
                OrderStatus         = CampoMainQ.item("orderStatus")
                TotalAmount         = replace(CampoMainQ.item("totalGross"), "#", ",")
                TotalFreight        = replace(CampoMainQ.item("totalFreight"), "#", ",")
                StoreName           = "NetShoes"

                PedidoTroca        = CampoMainQ.item("exchangeRequested") 'verificar se pedido é uma troca (var retorna bool)

                BoolPedidoCancelado = false
       
                if(TotalFreight = "" or isnull(TotalFreight))   then  TotalFreight = 0
                if(TotalAmount = "" or isnull(TotalAmount))     then  TotalAmount = 0


                For Each subItem4 In oJSONLista.data("items").item(PedidoQ).item("shippings")

                    Set CampoMainItens  = oJSONLista.data("items").item(PedidoQ).item("shippings").item(subItem4)
   
                    cancellationReason = CampoMainItens.item("cancellationReason")

                    if(cancellationReason = "" or isnull(cancellationReason) or cancellationReason = "null" or cancellationReason = "string") then
                        country             = CampoMainItens.item("country")
                        shippingEstimate    = CampoMainItens.item("shippingEstimate")
                        deliveryTime        = CampoMainItens.item("deliveryTime")
                        freightAmount       = CampoMainItens.item("freightAmount")
                        shippingCode        = CampoMainItens.item("shippingCode")
                        status              = CampoMainItens.item("status")
    
                        Set CampoMaincustomer  = CampoMainItens.item("customer")

                        phones1C            = CampoMaincustomer.item("cellPhone")
                        phones2C            = CampoMaincustomer.item("landLine")
                        emailC              = ""
                        nameC               = CampoMaincustomer.item("customerName") '---'***linha 7*** 'URLDecode2(Unescape(CampoMaincustomer.item("customerName"))) 
                        vat_numberC         = CampoMaincustomer.item("document")
                        landLine            = CampoMaincustomer.item("landLine")
                        recipientName       = CampoMaincustomer.item("recipientName")
                        stateInscription    = CampoMaincustomer.item("stateInscription")
                        tradeName           = CampoMaincustomer.item("tradeName")
                  
                        Set CampoMaincustomeraddress  = CampoMaincustomer.item("address")

                        cityS           = CampoMaincustomeraddress.item("city") '---'***linha 7*** ''URLDecode2(Unescape(replace(replace(CampoMaincustomeraddress.item("city"),"'"," "),""""," "))) 
                        detailS         = CampoMaincustomeraddress.item("complement")
                        neighborhoodS   = URLDecode2(Unescape(replace(replace(CampoMaincustomeraddress.item("neighborhood"),"'"," "),""""," ")))
                        numberS         = URLDecode2(Unescape(replace(replace(CampoMaincustomeraddress.item("number"),"'"," "),""""," ")))
                        postcodeS       = URLDecode2(Unescape(replace(replace(CampoMaincustomeraddress.item("postalCode"),"'"," "),""""," ")))
                        reference       = CampoMaincustomeraddress.item("reference")
                        countryS        = URLDecode2(Unescape(replace(replace(CampoMaincustomeraddress.item("state"),"'"," "),""""," ")))
                        streetS         = URLDecode2(Unescape(replace(replace(CampoMaincustomeraddress.item("street"),"'"," "),""""," ")))

                        Set CampoMaincustomeraddress  = nothing
                        Set CampoMaincustomer  = nothing
    
                        lCont               = -1 

                        For Each lContitems In CampoMainItens.item("items")
                            lCont = lCont + 1

                            Set CampoMainItensshippings  = CampoMainItens.item("items").item(lContitems)

                            ArrayItens(lCont, 0)      = CampoMainItens.item("items").item(lContitems).item("sku") 
                            ArrayItens(lCont, 1)      = CampoMainItens.item("items").item(lContitems).item("quantity")
                            ArrayItens(lCont, 2)      = FormatNumber(replace(CampoMainItens.item("items").item(lContitems).item("totalNet"),"#",","),2)
   
                            if(lcase(trim(CampoMainItens.item("items").item(lContitems).item("totalDiscount"))) = "0" or lcase(trim(CampoMainItens.item("items").item(lContitems).item("totalDiscount"))) = "null" or isnull(CampoMainItens.item("items").item(lContitems).item("totalDiscount")) ) then 
                                ArrayItens(lCont, 3)      = 0
                            else
                                ArrayItens(lCont, 3)      = FormatNumber(replace(CampoMainItens.item("items").item(lContitems).item("totalDiscount"),"#",","),2)
                            end if
                        
                            ArrayItens(lCont, 4)       = CampoMainItens.item("items").item(lContitems).item("brand")
                            ArrayItens(lCont, 5)       = CampoMainItens.item("items").item(lContitems).item("color")
                            ArrayItens(lCont, 6)       = CampoMainItens.item("items").item(lContitems).item("departmentCode")
                            ArrayItens(lCont, 7)       = CampoMainItens.item("items").item(lContitems).item("departmentName")
                            ArrayItens(lCont, 8)       = CampoMainItens.item("items").item(lContitems).item("discountUnitValue")
                            ArrayItens(lCont, 9)       = CampoMainItens.item("items").item(lContitems).item("ean")
                            ArrayItens(lCont, 10)      = CampoMainItens.item("items").item(lContitems).item("flavor")
                            ArrayItens(lCont, 11)      = CampoMainItens.item("items").item(lContitems).item("grossUnitValue")
                            ArrayItens(lCont, 12)      = CampoMainItens.item("items").item(lContitems).item("itemId")
                            ArrayItens(lCont, 13)      = CampoMainItens.item("items").item(lContitems).item("manufacturerCode")
                            ArrayItens(lCont, 14)      = CampoMainItens.item("items").item(lContitems).item("name")
                            ArrayItens(lCont, 15)      = CampoMainItens.item("items").item(lContitems).item("netUnitValue")
                            ArrayItens(lCont, 16)      = CampoMainItens.item("items").item(lContitems).item("size")
                            ArrayItens(lCont, 17)      = CampoMainItens.item("items").item(lContitems).item("status")
                            ArrayItens(lCont, 18)      = CampoMainItens.item("items").item(lContitems).item("totalCommission")
                            ArrayItens(lCont, 19)      = CampoMainItens.item("items").item(lContitems).item("totalFreight")
                            ArrayItens(lCont, 20)      = CampoMainItens.item("items").item(lContitems).item("totalGross")

                            if(ArrayItens(lCont, 0) <> "") then 
                                Cdcliente = TRIM(Recupera_Campos_Db_oConn("TB_PRODUTOS", ArrayItens(lCont, 0), "id", "CDCLIENTE")) 'sku
                            end if
                            Set CampoMainItensshippings  = nothing

                            if(Cdcliente = "" or isnull(Cdcliente)) then Cdcliente = Chave
                        Next
                    else
                        BoolPedidoCancelado = true
                    end if    
                    Set CampoMainItens  = nothing
                Next
    
                Id_Status               = DeParaStatus(OrderStatus)
                BoolPedidoErroAcento    = false

                Set CampoMainQ      = nothing
  
                'response.write "Cdcliente       " & Cdcliente       & "<br>"
                'response.write "StoreName       " & StoreName       & "<br>"
                'response.write "Id_Status       " & Id_Status       & "<br>"
                'response.write "InsertedDate    " & InsertedDate    & "<br>"
                'response.write "OrderStatus     " & OrderStatus     & "<br>"
                'response.write "ArrayItens(0, 0) " & IsNumeric(ArrayItens(0, 0))     & "<br>"
                'response.End
    
                if(InStr(lcase(code), "t") > 0 ) then 
                    BoolPedidoCancelado = true
                end if

                if( (Cdcliente <> "" and StoreName <> "" and Id_Status <> "") and (BoolPedidoCancelado = false)  and (BoolPedidoErroAcento = false) and IsNumeric(ArrayItens(0, 0)) ) then 
                    Id_FrmPagamento     = "91"
                    Id_Tipo             = ""
                    Entrega_Diferente   = "0"
                    shipping_cost       = 0
                    sEstado             = trim(Recupera_Campos_Db_oConn("TB_ESTADOS", countryS, "Sigla", "id"))
                    
                    if(vat_numberC <> "") then vat_numberC = replace(replace(vat_numberC, ".", ""), "-", "")

			        sSqlCidade = "SELECT  top 1  descricao, Trasportadora FROM TB_TIPO_ENTREGA (nolock) WHERE (CdCliente = "& Cdcliente &") and ativo = 1 order by id "
			        set RsCidade = oConn.execute(sSqlCidade)
			        if (not RsCidade.eof) then
                        TipoEntregaDescricao        = trim(RsCidade("descricao"))
                        TipoEntregaTransportadora   = trim(RsCidade("Trasportadora"))
			        end if
			        set RsCidade = nothing
   
                    ' ========================================================================================
                    sSql = "SELECT TOP 1 ID from TB_CLIENTES (NOLOCK) WHERE cpf = '"& vat_numberC &"' and cdcliente = " & Cdcliente
	                Set Rs = oConn.execute(sSql)

	                if(rs.eof) then
                        sSQlQtdTotal = " SELECT top 1 P.ID FROM TB_CLIENTES_TIPO P (NOLOCK) WHERE lower(rtrim(ltrim(P.Descricao))) like '%"& lcase(trim(StoreName)) &"%' and cdcliente = " & Cdcliente & " order by p.id"
                        set RsQtdTotal2 = oConn.execute(sSQlQtdTotal)

                        if(RsQtdTotal2.eof) then
		                    ' INSERI LINHA NA TABELA HISTORICO ------------------------------
		                    sSQlI = "INSERT INTO TB_CLIENTES_TIPO (CdCliente, Descricao, Ativo) VALUES ('"& Cdcliente &"', 'Consumidores "& trim(StoreName) &"', 1) "
		                    oConn.execute(sSQlI)

                            sSqlInsert      = "SELECT @@IDENTITY AS 'Id'"
                            Set RsIn = oConn.execute(sSqlInsert)
                            if(not RsIn.eof) then Id_Tipo = RsIn("id")
                            Set RsIn = nothing
                        else
                            Id_Tipo = RsQtdTotal2("id")
                        end if
                        set RsQtdTotal2 = nothing

                        sSqlInsert = " INSERT INTO TB_CLIENTES (Cdcliente, Id_Tipo, Id_Provincia, Nome, Endereco, CEP, Numero, Cidade, Bairro, Complemento, Email, Senha, CPF, TipoPessoa, Telefone, Pais, TipoEndereco, Celular, Ativo) " & _ 
                                        " VALUES ('"& Cdcliente &"','"& Id_Tipo &"', '"& sEstado &"', '"& Replace(nameC,"'","") &"','"& streetS &"', '"& postcodeS &"', '"& numberS &"', '"& cityS &"', '"& neighborhoodS &"', '"& detailS &"', '"& emailC &"','123456','"& vat_numberC &"','F','"& phones1C &"','BR', 'R', '"& phones2C &"', 1) "
                        oConn.execute(sSqlInsert)

                        sSqlInsert      = "SELECT @@IDENTITY AS 'Id'"
                        Set RsIn = oConn.execute(sSqlInsert)
                        if(not RsIn.eof) then sIdProdCad = RsIn("id")
                        Set RsIn = nothing
                    else
                        sIdProdCad = rs("id")
                    end if
	                Set Rs = nothing

                    ' ========================================================================================
                    if(sIdProdCad <> "") then 

                        sSqlVer = "SELECT TOP 1 ID, id_status from TB_PEDIDOS (NOLOCK) WHERE lower(rtrim(ltrim(CodigoServicoIG))) = '"& TRIM(lcase(IdOrderMarketplace)) &"' and Id_FrmPagamento = " & Id_FrmPagamento
                        Set RsVer = oConn.execute(sSqlVer)

	                    if(RsVer.eof) then
                            sqlPedido	=	"INSERT INTO Tb_Pedidos ( " & _
						                            "CdCliente,	" & _
						                            "Id_Status,	" & _
						                            "DataCad,	" & _
						                            "VeioDoBanner,	" & _
						                            "Id_Cliente,	" & _
						                            "Id_Provincia,	" & _
						                            "Id_FrmPagamento,	" & _
						                            "Entrega_Diferente,	" & _
						                            "Endereco,	" & _
						                            "Cep,	" & _
						                            "Cidade,	" & _
						                            "Bairro,	" & _
                                                    "Complemento,	" & _
						                            "Numero,	" & _
						                            "Pais,	" & _
						                            "SubTotal,	" & _
						                            "Total,	" & _
						                            "ValorParcela," & _
						                            "FreteReal,	" & _
						                            "Frete,	" & _
						                            "Parcelamento,	" & _
                                                    "TipoEntregaDescricao,	" & _
                                                    "TipoEntregaTransportadora,	" & _
                                                    "CodigoServicoIG,	" & _
                                                    "RetornoIntegracao2,	" & _
                                                    "RetornoIntegracao3,	" & _
                                                    "RetornoIntegracao5,	" & _
                                                    "RetornoBcash	" & _   
					                        ") VALUES (" & _
                                                Cdcliente &"," & _
						                        " "& Id_Status &"," & _
						                        " getdate(),                                  " & _
						                        "'" & SafeSQL(Trachannel)					& "',   " & _
						                        "'" & SafeSQL(sIdProdCad)   				& "',   " & _
						                        "'" & SafeSQL(sEstado)           			& "',   " & _
						                        "'"& SafeSQL(Id_FrmPagamento)               &"',    " & _
						                        "'"& SafeSQL(Entrega_Diferente)             &"',    " & _
						                        "'" & SafeSQL(streetS)       				& "',   " & _
						                        "'" & SafeSQL(postcodeS)      				& "',   " & _
						                        "'" & SafeSQL(cityS)   					    & "',   " & _
						                        "'" & SafeSQL(neighborhoodS)   				& "',   " & _
                                                "'" & SafeSQL(detailS)   				    & "',   " & _
						                        "'" & SafeSQL(numberS)   					& "',   " & _
						                        "'BR',                                      " & _
						                        "'" &   Replace(Replace((cdbl(TotalAmount) - cdbl(TotalFreight)),".",""),",",".") &"',  " & _
						                        "'" &   Replace(Replace(TotalAmount,".",""),",",".") &"', " & _
                                                "'" &   Replace(Replace(TotalAmount,".",""),",",".") &"', " & _
						                        "'" &   Replace(Replace(TotalFreight,".",""),",",".") &"', " & _
                                                "'" &   Replace(Replace(TotalFreight,".",""),",",".") &"', " & _
						                        "1 ,                                        " & _
                                                "'"& TipoEntregaDescricao       &"',                   " & _
                                                "'"& TipoEntregaTransportadora  &"',                   " & _
                                                "'"& Code &"',                              " & _
                                                "'"& IdOrderMarketplace &"',                " & _
                                                "'"& Id &"',                                " & _
                                                "'"& shippingCode &"',                                " & _
                                                "'"& replace(sRetorno, "'", "") &"'                           " & _
						                        ")"
    
                            oConn.execute(sqlPedido)

                            if(Err.number <> 0) then 
                                Response.Write "<font color=red>["& sqlPedido &"<br><br>"& Err.Description &"<br><br>"& Err.number &"<br><br>"& Err.Source &"]</font>"
                                Response.End
                                on error goto 0
                            end if

	                        sSql 	= "SELECT @@IDENTITY AS 'Id'"
	                        set Rs2 = oConn.execute(sSql)
	                        if(not Rs2.eof) then Itens_Id_Pedido = rs2("id")
	                        set Rs2 = nothing
        
	                        ' INSERI LINHA NA TABELA HISTORICO ------------------------------
	                        sSQl = "INSERT INTO TB_HISTORICOS (Id_Pedido, Id_Status, Descricao) VALUES ('"& Itens_Id_Pedido &"', 1, '"& TRIM(Recupera_Campos_Db_oConn("TB_STATUS", "1", "id", "DESCRICAO")) &" [Robo NetShoes Pedidos]')"
	                        oConn.execute(sSQl)

                            if(Err.number <> 0) then 
                                Response.Write "<font color=red>["& sSQl &" - "& Err.Description &" - "& Err.number &" - "& Err.Source &"]</font>"
                                Response.End
                                on error goto 0
                            end if

                            if(Id_Status <> 1) then 
	                            sSQl = "INSERT INTO TB_HISTORICOS (Id_Pedido, Id_Status, Descricao) VALUES ('"& Itens_Id_Pedido &"', "& Id_Status &", '"& TRIM(Recupera_Campos_Db_oConn("TB_STATUS", Id_Status, "id", "DESCRICAO")) &" [Robo NetShoes Pedidos]')"
	                            oConn.execute(sSQl)

                                if(Err.number <> 0) then 
                                    Response.Write "<font color=red>["& sSQl &" - "& Err.Description &" - "& Err.number &" - "& Err.Source &"]</font>"
                                    Response.End
                                    on error goto 0
                                end if
                            end if
                             
                            sTotalSubtotal          = 0
                            sDiferencaValores       = 0
                            sDiferencaValoresPor    = 0

                            if(cdbl(sTotalSubtotal) > cdbl(total_ordered)) then '---'***verificar total_ordered***
                                sDiferencaValores       = FormatNumber((cdbl(sTotalSubtotal) - cdbl(total_ordered)),2)
                                sDiferencaValoresPor    = (((cdbl(total_ordered) / cdbl(sTotalSubtotal)) - 1) * 100)

                                if(cdbl(sDiferencaValores) > 0) then 
                                    if(cdbl(sDiferencaValoresPor) < 0) then 
                                        sDiferencaValoresPor = FormatNumber((sDiferencaValoresPor * (-1)),0)
                                    end if
                                end if
                            end if

                            for lContItem = 0 to lCont                           
                                sValorProdutoItemDescontoBoleto = 0
                                sValorProdutoItemDesconto       = 0        
    
                                if(cdbl(sDiferencaValoresPor) > 0 ) then 
                                    sValorProdutoItemDescontoBoleto = ((cdbl(sDiferencaValoresPor) /100) * cdbl(ArrayItens(lContItem, 5)))

                                    if(cdbl(sValorProdutoItemDescontoBoleto) > 0) then 
                                        ArrayItens(lContItem, 5)    = FormatNumber(( cdbl(ArrayItens(lContItem, 5)) - cdbl(sValorProdutoItemDescontoBoleto) ),2)

                                        sValorProdutoItemDesconto   =  FormatNumber((cdbl(sValorProdutoItemDesconto) + cdbl(sValorProdutoItemDescontoBoleto)),2)
                                    end if
                                end if                              

	                            sSQl = "Insert Into TB_PEDIDOS_ITENS ("                          & _
				                            " Id_Pedido, "                                       & _
				                            " Id_Produto, "                                      & _
				                            " Id_Moeda, "                                        & _
				                            " Preco_Unitario,  "                                 & _
				                            " Preco_Unitario_Bruto,  "                           & _
                                            " Preco_Unitario_Desconto,  "                        & _
				                            " Quantidade) "                                      & _ 
			                            " values ("                                              & _
					                            " '"&	Itens_Id_Pedido				         &"', " & _
					                            " '"&	ArrayItens(lContItem, 0) 	         &"', " & _
					                            "  "&   sIdMoedda                            &"," & _
					                            " '"&	Replace(Replace(replace(ArrayItens(lContItem, 15), "#", ","),".",""),",",".")  &"', " & _
					                            " '"&	Replace(Replace(replace(ArrayItens(lContItem, 20), "#", ","),".",""),",",".") 	&"', " & _
                                                " '"&	Replace(Replace(replace(ArrayItens(lContItem, 8), "#", ","),".",""),",",".") 	&"', " & _
					                            " "&    ArrayItens(lContItem, 1)    &")"
		                        oConn.execute(sSQl)

                                if(Err.number <> 0) then 
                                    Response.Write "<font color=red>["& sSQl &" - "& Err.Description &" - "& Err.number &" - "& Err.Source &"]</font>"
                                    Response.End
                                    on error goto 0
                                end if
                                                         
                                sTotalSubtotal = (cdbl(sTotalSubtotal) + cdbl(ArrayItens(lContItem, 15)) )
                    
                                call Subtrai_Estoque(ArrayItens(lContItem, 0), ArrayItens(lContItem, 1))
                            next

                            if(InStr(lcase(code), "t") > 0 ) then 
                                ' VERICICAR PEDIDO TROCA --------------------------------------------------------------------------
                                sSqlVerTroca = "SELECT TOP 1 ID from TB_PEDIDOS (NOLOCK) WHERE lower(rtrim(ltrim(CodigoServicoIG))) = '"& replace(TRIM(lcase(code)), "t", "") &"' and cdcliente = " & Cdcliente
                                Set RsVerTroca = oConn.execute(sSqlVerTroca)
	                            if(not RsVerTroca.eof) then
                                    sIdPedidoOriginal = RsVerTroca("id")

                                    ' MUDAR O STATUS DO PEDIDO ORIGINAL ----------------------------------
			                        oConn.execute("Update TB_PEDIDOS set ID_STATUS = 16 where id = " & sIdPedidoOriginal)

	                                ' INSERI LINHA NA TABELA HISTORICO ------------------------------
	                                sSQl = "INSERT INTO TB_HISTORICOS (Id_Pedido, Id_Status, Descricao) VALUES ('"& sIdPedidoOriginal &"', 1, '"& TRIM(Recupera_Campos_Db_oConn("TB_STATUS", "1", "id", "DESCRICAO")) &" [Pedido Novo de Troca: "& Itens_Id_Pedido &"]')"
	                                oConn.execute(sSQl)
                                end if
                                Set RsVerTroca = nothing
                                ' VERICICAR PEDIDO TROCA --------------------------------------------------------------------------
                            end if

                            response.write "Pedido INSERIDO: "& Itens_Id_Pedido &" - "& sSqlVer &"<br/>"
                            response.Write "<hr>"
                            Response.flush
                        else
                            response.write "<br/>Pedido DUPLICADO (não inserido): "& trim(Code) &" - "& RsVer("id") &" - "& sSqlVer &"<br/>"    
    
                            id_statusCad = trim(RsVer("id_status"))

                            if(id_statusCad <> trim(Id_Status)) then 
                                if(id_statusCad = "1"  and trim(Id_Status) = "2") then
	                                sSQl = "INSERT INTO TB_HISTORICOS (Id_Pedido, Id_Status, Descricao) VALUES ('"& trim(RsVer("id")) &"', "& Id_Status &", '"& TRIM(Recupera_Campos_Db_oConn("TB_STATUS", Id_Status, "id", "DESCRICAO")) &" [Robo NetShoes Pedidos]')"
	                                oConn.execute(sSQl)                               

                                    ' MUDAR O STATUS DO PEDIDO ORIGINAL ----------------------------------
			                        oConn.execute("Update TB_PEDIDOS set ID_STATUS = "& Id_Status &" where id = " & trim(RsVer("id")))

                                    response.write "<br/>Pedido ATUALIZADO status (não inserido): "& trim(Code) &" - "& RsVer("id") &" - "& sSqlVer &"<br/>"    
                                end if
                            end if

                            ' PEDIDO EXISTENTE **************************************************
                            response.write "<br/>Pedido DUPLICADO (não inserido): "& trim(Code) &" - "& RsVer("id") &" - "& sSqlVer &"<br/>"    
                            response.Write "<hr>"
                            Response.flush
                        end if
                        Set RsVer = nothing
                    end if

                else
                    ' if( (Cdcliente <> "" and StoreName <> "" and Id_Status <> "") and (BoolPedidoCancelado = false)  and (BoolPedidoErroAcento = false) and IsNumeric(ArrayItens(0, 0)) ) then             
                    response.Write "<br/>[ERRO Cdcliente, StoreName, Id_Status, BoolPedidoCancelado, BoolPedidoErroAcento]"
                    response.Write "<br/>Cdcliente: " & Cdcliente
                    response.Write "<br/>StoreName: " & StoreName
                    response.Write "<br/>OrderStatus: " & OrderStatus
                    response.Write "<br/>Id_Status: " & Id_Status
                    response.Write "<br/>BoolPedidoCancelado ou TROCA: " & BoolPedidoCancelado
                    response.Write "<br/>BoolPedidoErroAcento: " & BoolPedidoErroAcento
                    response.Write "<br/>code: " & code
                    response.Write "<br/>IdOrderMarketplace: " & IdOrderMarketplace
                    response.Write "<br/>ArrayItens(0, 0): " & ArrayItens(0, 0)
                    response.Write "<br/><hr>"
                    Response.flush
                end if         

                call MyDelay(2)
            next
        end if
        Set oJSONLista = nothing
    end if

    call MyDelay(3)

    sRetorno = Retornar_Json_MKT(lContPaginas)
next

sSQlU = "exec dbo.sp_VerificaPedidoRepetidoNetShoes"
oConn.execute(sSQlU)

call Fecha_Conexao()


' =======================================================================================================
Function RandNo()
    Randomize
    RandNo = Int(99999 * Rnd + 3)
End Function


' =======================================================================================================
Sub MyDelay(NumberOfSeconds)
    Dim DateTimeResume
    DateTimeResume= DateAdd("s", NumberOfSeconds, Now())
    Do Until (Now() > DateTimeResume)
    Loop
End Sub


' =======================================================================
function DeParaStatus(prlabelS)

    if(prlabelS = "" or prlabelS = "null") then 
        DeParaStatus = "1"
        exit function
    end if

    select case trim(ucase(prlabelS))
        case "CREATED"
            DeParaStatus = "1"
            exit function
        case "APPROVED"
            DeParaStatus = "2"
            exit function
        case "DELIVERED"
            DeParaStatus = "4"
            exit function
        case "INVOICED"
            DeParaStatus = "56"
            exit function
        case "SHIPPED"
            DeParaStatus = "6"
            exit function
    end select 
end function 




' =======================================================================
sub Subtrai_Estoque(IdProduto, nQtde)

	if(IdProduto = "" OR not IsNumeric(IdProduto) ) then exit sub
	
	dim sSql, Rs, Rs1, sEstoqueAtual

	sSql = "Select top 1 Qte_Estoque, Qte_Estoque_MKT, BoolBundle, BoolMktPlace from TB_PRODUTOS (nolock) where id = " & IdProduto
	set Rs = oConn.execute(sSql)
	
	if(not Rs.eof) then
		sEstoqueAtual   = rs("Qte_Estoque")
        Qte_Estoque_MKT = rs("Qte_Estoque_MKT")
        BoolMktPlace    = rs("BoolMktPlace")
		
		if(sEstoqueAtual <> "0" and sEstoqueAtual <> "" and not isnull(sEstoqueAtual)) then 
			sEstoqueAtual = (cdbl(sEstoqueAtual) - cdbl(nQtde))
			if(sEstoqueAtual < 0) then sEstoqueAtual = 0
			
			'oConn.execute("Update TB_PRODUTOS set Qte_Estoque = "& sEstoqueAtual &" where id = " & IdProduto)
			
			if(trim(rs("BoolBundle")) = "1") then
				sSql = "Select id_produto_bundle from TB_PRODUTOS_BUNDLE (nolock) where id_produto = " & IdProduto
				set Rs1 = oConn.execute(sSql)
				if(not Rs1.eof) then
					do while not Rs1.eof
						sEstoqueAtual = rs("Qte_Estoque")
						sEstoqueAtual = (cdbl(sEstoqueAtual) - cdbl(nQtde))
						'oConn.execute("Update TB_PRODUTOS set Qte_Estoque = "& sEstoqueAtual &" where id = " & Rs1("id_produto_bundle"))
					Rs1.movenext
					loop
				end if
				set Rs1 = nothing
			end if
		end if

        if(BoolMktPlace = "1") then 
		    if(Qte_Estoque_MKT <> "0" and Qte_Estoque_MKT <> "" and not isnull(Qte_Estoque_MKT)) then 
			    Qte_Estoque_MKT = (cdbl(Qte_Estoque_MKT) - cdbl(nQtde))
			    if(Qte_Estoque_MKT < 0) then Qte_Estoque_MKT = 0
			
			    'oConn.execute("Update TB_PRODUTOS set Qte_Estoque_MKT = "& Qte_Estoque_MKT &" where id = " & IdProduto)
			
			    if(trim(rs("BoolBundle")) = "1") then
				    sSql = "Select id_produto_bundle from TB_PRODUTOS_BUNDLE (nolock) where id_produto = " & IdProduto
				    set Rs1 = oConn.execute(sSql)
				    if(not Rs1.eof) then
					    do while not Rs1.eof
						    sEstoqueAtual = rs("Qte_Estoque_MKT")
						    sEstoqueAtual = (cdbl(sEstoqueAtual) - cdbl(nQtde))
						    'oConn.execute("Update TB_PRODUTOS set Qte_Estoque_MKT = "& Qte_Estoque_MKT &" where id = " & Rs1("id_produto_bundle"))
					    Rs1.movenext
					    loop
				    end if
				    set Rs1 = nothing
			    end if
		    end if
        end if

	end if
	set Rs = nothing
end sub


FUNCTION URLDecode2(str)
'// This function:
'// - decodes any utf-8 encoded characters into unicode characters eg. (%C3%A5 = å)
'// - replaces any plus sign separators with a space character
'//
'// IMPORTANT:
'// Your webpage must use the UTF-8 character set. Easiest method is to use this META tag:
'// <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
'//
      
    str = replace(str, "%", "")

    Dim objScript

    Set objScript = Server.CreateObject("MSScriptControl.ScriptControl")
    ' Set objScript = Server.CreateObject("msscript")
    objScript.Language = "JavaScript"
    URLDecode2 = objScript.Eval("decodeURIComponent(""" & str & """.replace(/\+/g,"" ""))")
    Set objScript = NOTHING

	on error goto 0
END FUNCTION


Function Base64Encode(inData)
    'rfc1521
    '2001 Antonin Foller, Motobit Software, http://Motobit.cz

    Const Base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    Dim cOut, sOut, I

    'For each group of 3 bytes

    For I = 1 To Len(inData) Step 3
    Dim nGroup, pOut, sGroup

    'Create one long from this 3 bytes.

    nGroup = &H10000 * Asc(Mid(inData, I, 1)) + _
    &H100 * MyASC(Mid(inData, I + 1, 1)) + MyASC(Mid(inData, I + 2, 1))

    'Oct splits the long To 8 groups with 3 bits

    nGroup = Oct(nGroup)

    'Add leading zeros

    nGroup = String(8 - Len(nGroup), "0") & nGroup

    'Convert To base64

    pOut = Mid(Base64, CLng("&o" & Mid(nGroup, 1, 2)) + 1, 1) + _
    Mid(Base64, CLng("&o" & Mid(nGroup, 3, 2)) + 1, 1) + _
    Mid(Base64, CLng("&o" & Mid(nGroup, 5, 2)) + 1, 1) + _
    Mid(Base64, CLng("&o" & Mid(nGroup, 7, 2)) + 1, 1)

    'Add the part To OutPut string

    sOut = sOut + pOut

    'Add a new line For Each 76 chars In dest (76*3/4 = 57)
    'If (I + 2) Mod 57 = 0 Then sOut = sOut + vbCrLf

    Next

    Select Case Len(inData) Mod 3
        Case 1: '8 bit final
            sOut = Left(sOut, Len(sOut) - 2) + "=="
        Case 2: '16 bit final
            sOut = Left(sOut, Len(sOut) - 1) + "="
    End Select

    Base64Encode = sOut
End Function

Function MyASC(OneChar)
    If OneChar = "" Then MyASC = 0 Else MyASC = Asc(OneChar)
End Function


Function Base64Decode(ByVal base64String)
    'rfc1521
    '1999 Antonin Foller, Motobit Software, http://Motobit.cz

    Const Base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    Dim dataLength, sOut, groupBegin

    'remove white spaces, If any

    base64String = Replace(base64String, vbCrLf, "")
    base64String = Replace(base64String, vbTab, "")
    base64String = Replace(base64String, " ", "")

    'The source must consists from groups with Len of 4 chars

    dataLength = Len(base64String)
    If dataLength Mod 4 <> 0 Then
    Err.Raise 1, "Base64Decode", "Bad Base64 string."
        Exit Function
    End If


    ' Now decode each group:

    For groupBegin = 1 To dataLength Step 4
    Dim numDataBytes, CharCounter, thisChar, thisData, nGroup, pOut
    ' Each data group encodes up To 3 actual bytes.

    numDataBytes = 3
    nGroup = 0

    For CharCounter = 0 To 3
    ' Convert each character into 6 bits of data, And add it To
    ' an integer For temporary storage. If a character is a '=', there
    ' is one fewer data byte. (There can only be a maximum of 2 '=' In
    ' the whole string.)

    thisChar = Mid(base64String, groupBegin + CharCounter, 1)

    If thisChar = "=" Then
        numDataBytes = numDataBytes - 1
        thisData = 0
    Else
        thisData = InStr(1, Base64, thisChar, vbBinaryCompare) - 1
    End If
    If thisData = -1 Then
        Err.Raise 2, "Base64Decode", "Bad character In Base64 string."
        Exit Function
    End If

        nGroup = 64 * nGroup + thisData
    Next

    'Hex splits the long To 6 groups with 4 bits

    nGroup = Hex(nGroup)

    'Add leading zeros

    nGroup = String(6 - Len(nGroup), "0") & nGroup

    'Convert the 3 byte hex integer (6 chars) To 3 characters

    pOut = Chr(CByte("&H" & Mid(nGroup, 1, 2))) + _
    Chr(CByte("&H" & Mid(nGroup, 3, 2))) + _
    Chr(CByte("&H" & Mid(nGroup, 5, 2)))

    'add numDataBytes characters To out string

    sOut = sOut & Left(pOut, numDataBytes)
    Next

    Base64Decode = sOut
End Function



' =======================================================================================
function Retornar_Json_MKT(prPage)

    dim sRetornoF, oJSONLista2

    UrlApiProduto0 = "http://api-marketplace.netshoes.com.br/api/v1/orders?page="& prPage &"&size=20&expand=items,shippings&orderStatus="
    'UrlApiProduto0 = "http://api-marketplace.netshoes.com.br/api/v1/orders?page="& prPage &"&size=20&expand=items,shippings&orderStatus=Created"   ' criados
    'UrlApiProduto0  = "http://api-marketplace.netshoes.com.br/api/v1/orders?page="& prPage &"&size=20&expand=items,shippings&orderStatus=Approved"  ' Approved


    response.Write "<p>UrlApiProduto:"   & UrlApiProduto0 & "</br>"  
    response.Write "client_id: "         & LoginERP & "</br>"
    response.Write "access_token: "      & SenhaERP & "</p>"
    Response.flush

    Set oHttpRequest = Server.CreateObject("MSXML2.ServerXMLHTTP")
    with oHttpRequest 
        .Open "GET", UrlApiProduto0, false
        .SetRequestHeader "client_id", LoginERP
        .SetRequestHeader "access_token", SenhaERP
        .SetRequestHeader "idseller", IdSellerNetShoes
        .Send()
        sRetornoF        = .responseText
        sRetornoStatus  = .status
    End With 
    Set oHttpRequest = Nothing

    'sRetorno  = "{""items"":[{""expands"":[""shippings"",""items"",""devolutionItems""],""agreedDate"":1537498800000,""orderDate"":1536537478913,""orderNumber"":""11725710"",""totalQuantity"":1,""originSite"":""NETSHOES"",""orderStatus"":""Created"",""orderType"":""Sale"",""totalGross"":494.99,""totalCommission"":0,""totalDiscount"":0,""totalFreight"":145.99,""totalNet"":349,""shippings"":[{""expands"":[""items"",""devolutionItems""],""shippingCode"":771094432,""status"":""Created"",""shippingEstimate"":1537844400000,""deliveryTime"":12,""freightAmount"":145.99,""country"":""BR"",""transport"":{""expands"":[],""deliveryService"":""Normal"",""deliveryId"":""52"",""carrier"":""Total Express"",""carrierId"":""793""},""customer"":{""expands"":[],""document"":""07709327389"",""stateInscription"":"""",""customerName"":""Alexandre Uchoa Silva"",""recipientName"":""Alexandre Uchoa"",""tradeName"":"""",""cellPhone"":""988483770"",""landLine"":""988838958"",""address"":{""expands"":[],""neighborhood"":""Turu"",""postalCode"":""65066390"",""city"":""SAO LUIS"",""complement"":""rua 4 quadra h ipem turu"",""state"":""MA"",""stateName"":""MARANHAO"",""street"":""Outros Conjunto Solar dos Lusiadas"",""number"":""14"",""reference"":""prox ao ponto final do onibus""}},""sender"":{""expands"":[],""supplierCnpj"":""24873314000167"",""sellerCode"":7563,""sellerName"":""Ominic"",""supplierName"":""Ominic""},""items"":[{""expands"":[],""itemId"":405752953,""manufacturerCode"":""30"",""brand"":""Dream"",""name"":""Bicicleta Ergométrica Dream EX 500"",""quantity"":1,""sku"":""740234"",""departmentName"":""Fitness e Musculação"",""totalGross"":349,""totalCommission"":0,""totalDiscount"":0,""totalFreight"":145.99,""totalNet"":349,""grossUnitValue"":349,""discountUnitValue"":0,""netUnitValue"":349,""color"":""Preto+Prata"",""size"":""Único""}],""platformId"":""INTERNAL"",""serviceId"":""52"",""serviceName"":""Normal"",""links"":{""self"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11725710/shippings/771094432""},""order"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11725710""}}}],""links"":{""self"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11725710""},""shippings"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11725710/shippings""}}},{""expands"":[""shippings"",""items"",""devolutionItems""],""agreedDate"":1537498800000,""orderDate"":1536501411093,""orderNumber"":""11706071"",""totalQuantity"":1,""originSite"":""NETSHOES"",""orderStatus"":""Created"",""orderType"":""Sale"",""totalGross"":1614.99,""totalCommission"":0,""totalDiscount"":0,""totalFreight"":215.99,""totalNet"":1399,""shippings"":[{""expands"":[""items"",""devolutionItems""],""shippingCode"":771073401,""status"":""Created"",""shippingEstimate"":1537844400000,""deliveryTime"":12,""freightAmount"":215.99,""country"":""BR"",""transport"":{""expands"":[],""deliveryService"":""Normal"",""deliveryId"":""52"",""carrier"":""Total Express"",""carrierId"":""793""},""customer"":{""expands"":[],""document"":""09283232771"",""stateInscription"":"""",""customerName"":""Viviane Madalena"",""recipientName"":""Viviane"",""tradeName"":"""",""cellPhone"":"""",""landLine"":""991787018"",""address"":{""expands"":[],""neighborhood"":""Mangalô"",""postalCode"":""48060130"",""city"":""ALAGOINHAS"",""complement"":""A"",""state"":""BA"",""stateName"":""BAHIA"",""street"":""Rua Thompson Flores"",""number"":""261"",""reference"":""Proximo a capela Senhor do Bonfim""}},""sender"":{""expands"":[],""supplierCnpj"":""24873314000167"",""sellerCode"":7563,""sellerName"":""Ominic"",""supplierName"":""Ominic""},""items"":[{""expands"":[],""itemId"":405716003,""brand"":""Dream Fitness"",""name"":""Esteira Ergométrica Dream Concept 1.8"",""quantity"":1,""sku"":""740247"",""departmentName"":""Fitness e Musculação"",""totalGross"":1399,""totalCommission"":0,""totalDiscount"":0,""totalFreight"":215.99,""totalNet"":1399,""grossUnitValue"":1399,""discountUnitValue"":0,""netUnitValue"":1399,""color"":""Preto"",""size"":""Único""}],""platformId"":""INTERNAL"",""serviceId"":""52"",""serviceName"":""Normal"",""links"":{""self"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11706071/shippings/771073401""},""order"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11706071""}}}],""links"":{""self"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11706071""},""shippings"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11706071/shippings""}}},{""expands"":[""shippings"",""items"",""devolutionItems""],""agreedDate"":1537239600000,""orderDate"":1536280519307,""orderNumber"":""11636343"",""totalQuantity"":1,""originSite"":""NETSHOES"",""orderStatus"":""Created"",""orderType"":""Sale"",""totalGross"":431.99,""totalCommission"":0,""totalDiscount"":0,""totalFreight"":82.99,""totalNet"":349,""shippings"":[{""expands"":[""items"",""devolutionItems""],""shippingCode"":770992878,""status"":""Created"",""shippingEstimate"":1537844400000,""deliveryTime"":12,""freightAmount"":82.99,""country"":""BR"",""transport"":{""expands"":[],""deliveryService"":""Normal"",""deliveryId"":""52"",""carrier"":""Total Express"",""carrierId"":""793""},""customer"":{""expands"":[],""document"":""54680018020"",""stateInscription"":"""",""customerName"":""Mari Regina da Silva"",""recipientName"":""Mari Regina"",""tradeName"":"""",""cellPhone"":""995186998"",""landLine"":""997424488"",""address"":{""expands"":[],""neighborhood"":""Rio Branco"",""postalCode"":""96900000"",""city"":""SOBRADINHO"",""complement"":""Casa"",""state"":""RS"",""stateName"":""RIO GRANDE DO SUL"",""street"":""Rua Professor Ivo Rathcke"",""number"":""70""}},""sender"":{""expands"":[],""supplierCnpj"":""24873314000167"",""sellerCode"":7563,""sellerName"":""Ominic"",""supplierName"":""Ominic""},""items"":[{""expands"":[],""itemId"":405578946,""manufacturerCode"":""30"",""brand"":""Dream"",""name"":""Bicicleta Ergométrica Dream EX 500"",""quantity"":1,""sku"":""740234"",""departmentName"":""Fitness e Musculação"",""totalGross"":349,""totalCommission"":0,""totalDiscount"":0,""totalFreight"":82.99,""totalNet"":349,""grossUnitValue"":349,""discountUnitValue"":0,""netUnitValue"":349,""color"":""Preto+Prata"",""size"":""Único""}],""platformId"":""INTERNAL"",""serviceId"":""52"",""serviceName"":""Normal"",""links"":{""self"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11636343/shippings/770992878""},""order"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11636343""}}}],""links"":{""self"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11636343""},""shippings"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11636343/shippings""}}},{""expands"":[""shippings"",""items"",""devolutionItems""],""agreedDate"":1537239600000,""orderDate"":1536255624940,""orderNumber"":""11622295"",""totalQuantity"":1,""originSite"":""NETSHOES"",""orderStatus"":""Created"",""orderType"":""Sale"",""totalGross"":1138.99,""totalCommission"":0,""totalDiscount"":0,""totalFreight"":239.99,""totalNet"":899,""shippings"":[{""expands"":[""items"",""devolutionItems""],""shippingCode"":770976130,""status"":""Created"",""shippingEstimate"":1537844400000,""deliveryTime"":12,""freightAmount"":239.99,""country"":""BR"",""transport"":{""expands"":[],""deliveryService"":""Normal"",""deliveryId"":""52"",""carrier"":""Total Express"",""carrierId"":""793""},""customer"":{""expands"":[],""document"":""52951529104"",""stateInscription"":"""",""customerName"":""EDUARDO CESAR TORRES DA SILVA CESAR TORRES DA SILVA"",""recipientName"":""EDUARDO CESAR TORRES"",""tradeName"":"""",""cellPhone"":"""",""landLine"":""993642251"",""address"":{""expands"":[],""neighborhood"":""Cidade Vera Cruz"",""postalCode"":""74937400"",""city"":""APARECIDA DE GOIANIA"",""complement"":""QD 296 L 15 CIDADE VERA CRUZ"",""state"":""GO"",""stateName"":""GOIAS"",""street"":""Rua H 141"",""number"":""0""}},""sender"":{""expands"":[],""supplierCnpj"":""24873314000167"",""sellerCode"":7563,""sellerName"":""Ominic"",""supplierName"":""Ominic""},""items"":[{""expands"":[],""itemId"":405551058,""brand"":""Dream Fitness"",""name"":""Elíptico Magnético Dream black edition e"",""quantity"":1,""sku"":""772281"",""departmentName"":""Fitness e Musculação"",""totalGross"":899,""totalCommission"":0,""totalDiscount"":0,""totalFreight"":239.99,""totalNet"":899,""grossUnitValue"":899,""discountUnitValue"":0,""netUnitValue"":899,""color"":""Preto"",""size"":""Único""}],""platformId"":""INTERNAL"",""serviceId"":""52"",""serviceName"":""Normal"",""links"":{""self"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11622295/shippings/770976130""},""order"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11622295""}}}],""links"":{""self"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11622295""},""shippings"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11622295/shippings""}}},{""expands"":[""shippings"",""items"",""devolutionItems""],""agreedDate"":1537239600000,""orderDate"":1536255374600,""orderNumber"":""11622140"",""totalQuantity"":1,""originSite"":""NETSHOES"",""orderStatus"":""Created"",""orderType"":""Sale"",""totalGross"":1138.99,""totalCommission"":0,""totalDiscount"":0,""totalFreight"":239.99,""totalNet"":899,""shippings"":[{""expands"":[""items"",""devolutionItems""],""shippingCode"":770975928,""status"":""Created"",""shippingEstimate"":1537844400000,""deliveryTime"":12,""freightAmount"":239.99,""country"":""BR"",""transport"":{""expands"":[],""deliveryService"":""Normal"",""deliveryId"":""52"",""carrier"":""Total Express"",""carrierId"":""793""},""customer"":{""expands"":[],""document"":""52951529104"",""stateInscription"":"""",""customerName"":""EDUARDO CESAR TORRES DA SILVA"",""recipientName"":""EDUARDO"",""tradeName"":"""",""cellPhone"":"""",""landLine"":""993642251"",""address"":{""expands"":[],""neighborhood"":""Cidade Vera Cruz"",""postalCode"":""74937400"",""city"":""APARECIDA DE GOIANIA"",""complement"":""QD 296 L 15 CIDADE VERA CRUZ"",""state"":""GO"",""stateName"":""GOIAS"",""street"":""Rua H 141"",""number"":""0""}},""sender"":{""expands"":[],""supplierCnpj"":""24873314000167"",""sellerCode"":7563,""sellerName"":""Ominic"",""supplierName"":""Ominic""},""items"":[{""expands"":[],""itemId"":405550755,""brand"":""Dream Fitness"",""name"":""Elíptico Magnético Dream black edition e"",""quantity"":1,""sku"":""772281"",""departmentName"":""Fitness e Musculação"",""totalGross"":899,""totalCommission"":0,""totalDiscount"":0,""totalFreight"":239.99,""totalNet"":899,""grossUnitValue"":899,""discountUnitValue"":0,""netUnitValue"":899,""color"":""Preto"",""size"":""Único""}],""platformId"":""INTERNAL"",""serviceId"":""52"",""serviceName"":""Normal"",""links"":{""self"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11622140/shippings/770975928""},""order"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11622140""}}}],""links"":{""self"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11622140""},""shippings"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11622140/shippings""}}},{""expands"":[""shippings"",""items"",""devolutionItems""],""agreedDate"":1537239600000,""orderDate"":1536255236537,""orderNumber"":""11622052"",""totalQuantity"":1,""originSite"":""NETSHOES"",""orderStatus"":""Created"",""orderType"":""Sale"",""totalGross"":1138.99,""totalCommission"":0,""totalDiscount"":0,""totalFreight"":239.99,""totalNet"":899,""shippings"":[{""expands"":[""items"",""devolutionItems""],""shippingCode"":770975817,""status"":""Created"",""shippingEstimate"":1537844400000,""deliveryTime"":12,""freightAmount"":239.99,""country"":""BR"",""transport"":{""expands"":[],""deliveryService"":""Normal"",""deliveryId"":""52"",""carrier"":""Total Express"",""carrierId"":""793""},""customer"":{""expands"":[],""document"":""52951529104"",""stateInscription"":"""",""customerName"":""EDUARDO CESAR TORRES DA SILVA"",""recipientName"":""EDUARDO"",""tradeName"":"""",""cellPhone"":"""",""landLine"":""993642251"",""address"":{""expands"":[],""neighborhood"":""Cidade Vera Cruz"",""postalCode"":""74937400"",""city"":""APARECIDA DE GOIANIA"",""complement"":""QD 296 L 15 VERA CRUZ 7493400"",""state"":""GO"",""stateName"":""GOIAS"",""street"":""Rua H 141"",""number"":""0""}},""sender"":{""expands"":[],""supplierCnpj"":""24873314000167"",""sellerCode"":7563,""sellerName"":""Ominic"",""supplierName"":""Ominic""},""items"":[{""expands"":[],""itemId"":405550578,""brand"":""Dream Fitness"",""name"":""Elíptico Magnético Dream black edition e"",""quantity"":1,""sku"":""772281"",""departmentName"":""Fitness e Musculação"",""totalGross"":899,""totalCommission"":0,""totalDiscount"":0,""totalFreight"":239.99,""totalNet"":899,""grossUnitValue"":899,""discountUnitValue"":0,""netUnitValue"":899,""color"":""Preto"",""size"":""Único""}],""platformId"":""INTERNAL"",""serviceId"":""52"",""serviceName"":""Normal"",""links"":{""self"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11622052/shippings/770975817""},""order"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11622052""}}}],""links"":{""self"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11622052""},""shippings"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11622052/shippings""}}},{""expands"":[""shippings"",""items"",""devolutionItems""],""agreedDate"":1537239600000,""orderDate"":1536240101437,""orderNumber"":""11611934"",""totalQuantity"":1,""originSite"":""NETSHOES"",""orderStatus"":""Created"",""orderType"":""Sale"",""totalGross"":534.99,""totalCommission"":0,""totalDiscount"":0,""totalFreight"":145.99,""totalNet"":389,""shippings"":[{""expands"":[""items"",""devolutionItems""],""shippingCode"":770963663,""status"":""Created"",""shippingEstimate"":1537844400000,""deliveryTime"":12,""freightAmount"":145.99,""country"":""BR"",""transport"":{""expands"":[],""deliveryService"":""Normal"",""deliveryId"":""52"",""carrier"":""Total Express"",""carrierId"":""793""},""customer"":{""expands"":[],""document"":""70417188161"",""stateInscription"":"""",""customerName"":""joao vitor freitas santos"",""recipientName"":""joao vitor"",""tradeName"":"""",""cellPhone"":""996889203"",""landLine"":""998403726"",""address"":{""expands"":[],""neighborhood"":""SETOR DOS FUNCIONARIOS"",""postalCode"":""73900000"",""city"":""POSSE"",""state"":""GO"",""stateName"":""GOIAS"",""street"":""Rua 7 Qd 40 Lt 15A"",""number"":""0"",""reference"":""PANIFICADORA DELICIA""}},""sender"":{""expands"":[],""supplierCnpj"":""24873314000167"",""sellerCode"":7563,""sellerName"":""Ominic"",""supplierName"":""Ominic""},""items"":[{""expands"":[],""itemId"":405529968,""manufacturerCode"":""EX 550"",""brand"":""Dream"",""name"":""Bicicleta Ergométrica Dream Residencial EX 550"",""quantity"":1,""sku"":""740235"",""departmentName"":""Fitness e Musculação"",""totalGross"":389,""totalCommission"":0,""totalDiscount"":0,""totalFreight"":145.99,""totalNet"":389,""grossUnitValue"":389,""discountUnitValue"":0,""netUnitValue"":389,""color"":""Preto"",""size"":""Único""}],""platformId"":""INTERNAL"",""serviceId"":""52"",""serviceName"":""Normal"",""links"":{""self"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11611934/shippings/770963663""},""order"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11611934""}}}],""links"":{""self"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11611934""},""shippings"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders/11611934/shippings""}}}],""page"":0,""totalPages"":1,""size"":20,""total"":7,""links"":{""self"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders?page=0&size=20""},""first"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders?page=0&size=20""},""last"":{""href"":""http://prd-vs-mp.netshoes.local:8684/v1/orders?page=0&size=20""}}} "

    if(Err.number <> 0) then 
        Response.Write "<font color=red>["& Err.Description &" - "& Err.number &" - "& Err.Source &"]</font>"
        Response.End
        on error goto 0
    end if

    Set oJSONLista2 = New aspJSON
    oJSONLista2.loadJSON(replace(sRetornoF,".","#"))
    pageJson        = oJSONLista2.data("totalPages")
    Set oJSONLista2 = nothing

    response.Write "totalPages: "         & pageJson & "</br>"
    response.Write "<hr><br>"
    Response.flush

    'response.Write "<p>sRetorno: "          & sRetornoF & "</p>"
    'response.Write "<p>sRetornoStatus:"     & sRetornoStatus & "</p>" 
    'response.Write "<hr><br>"
    'response.End

    Retornar_Json_MKT = sRetornoF
end function 


' ==================================================================================================
Function SafeSQL(sInput)
  TempString = sInput
  sBadChars=array("select", "drop", ";", "--", "insert", "delete", "xp_", "#", "%", "&", "'", "(", ")", ":", ";", "<", ">", "=", "[", "]", "?", "`", "|") 
  For iCounter = 0 to uBound(sBadChars)
    TempString = replace(TempString,sBadChars(iCounter),"")
  Next
  SafeSQL = TempString
End function

%>