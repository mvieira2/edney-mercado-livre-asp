<!--#include file = "../../hidden/funcoes.asp"-->
<!--#include file = "../../hidden/aspJSON1.17/aspJSON1.17.asp"-->
<%
'---------GEST�O DE PRODUTOS------------------------------------------------------------------------------------
'-------------------------- GET /products/{sku}/status
'-Realiza o acompanhamento e valida��o de critica dos produtos criados
'------------------------------------------------------------------------------------------------------
Server.ScriptTimeOut = 65000
Response.Buffer      = true
Response.Charset     = "UTF-8"

Chave  = Replace_Caracteres_Especiais(Request("chave"))

if(Chave = "") then 
    response.write "[ERRO(1)]"
    response.End
end if

call Abre_Conexao()

Dim url, statusProduct, sRetornoCode, sRetorno, IdSellerNetShoes

call Abre_Conexao()

sSql =  "SELECT TB_PRODUTOS.Id, TB_PRODUTOS.descricao, LoginNetShoes, SenhaNetShoes, IdSellerNetShoes " & _
        " FROM  " & _
        "   TB_PRODUTOS (NOLOCK) " & _ 
        "   INNER JOIN TB_MARKETPLACES_ATRIBUTOS ON TB_PRODUTOS.ID = TB_MARKETPLACES_ATRIBUTOS.ID_PRODUTO  " & _
        "   INNER JOIN TBCLIENTE ON TB_PRODUTOS.CDCLIENTE = TBCLIENTE.CDCLIENTE  " & _
        " WHERE  1=1 "

if(Chave <> "" and IsNumeric(Chave)) then 
    sSql = sSql &  "   AND TB_PRODUTOS.CDCLIENTE = "& Chave
end if
    
Set Rs = oConn.execute(sSql)

' =======================================
Response.write sSql & "<hr><br>"
Response.flush
' =======================================
   
if(not Rs.eof) then

	do while not Rs.eof
        sku                 = trim(rs("ID"))
        descricao           = trim(rs("descricao"))
        LoginERP            = trim(rs("LoginNetShoes"))
        SenhaERP            = trim(rs("SenhaNetShoes"))
        IdSellerNetShoes    = trim(rs("IdSellerNetShoes"))

	    ' =======================================
	    Response.write "<h3>" & descricao & " ["& sku &"]</h3>"
	    ' =======================================

        getStatus(sku)
        readReviews(sku)     

	    ' =======================================
	    Response.write "<hr><br>"
	    Response.flush
	    ' =======================================
        
        'call MyDelay(2)
	Rs.movenext
	loop

else
	' =======================================
	Response.write "** sem produtos..<br>"
	Response.flush
	' =======================================
end if
Set Rs = nothing
call Fecha_Conexao()


' ===========================================================================================
Function getStatus(sku)  
    'url    = "http://api-sandbox.netshoes.com.br/api/v2/products/" & sku & "/status"
    url     = "http://api-marketplace.netshoes.com.br/api/v2/products/" & sku & "/status"

    Set oHttpRequest = CreateObject("MSXML2.ServerXMLHTTP")  
    with oHttpRequest    
        .Open "GET", url    
        .SetRequestHeader "Accept", "application/json; charset=utf-8"
        .SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
        .SetRequestHeader "client_id",      LoginERP
        .SetRequestHeader "access_token",   SenhaERP
        .SetRequestHeader "idseller", IdSellerNetShoes
        .Send()  
        sRetornoCode = .status
        sRetorno = .responseText  
    End With 
    Set oHttpRequest = Nothing   

    Set oJSONLista = New aspJSON

    oJSONLista.loadJSON(replace(sRetorno,".","#")) 
    statusProduct     = oJSONLista.data("status")

    oConn.execute("update TB_PRODUTOS set RetornoStatusNetShoes = '"& statusProduct &"' where id = " & sku)

    if(statusProduct <> "") then 
        Response.write statusProduct
    end if
End Function

'---------------------------------

'---Fnc L� erros de valida��o-----
Function readReviews(sky)   

    RetornoErroNetShoes = ""

    if(ucase(statusProduct) = "CRITICIZED") then 
        Set oJSONLista = New aspJSON
        oJSONLista.loadJSON(replace(sRetorno,".","#"))  

        For Each reviewsItems In oJSONLista.data("reviews")
            Set review     = oJSONLista.data("reviews").item(reviewsItems)

            reviewMsg      = review.item("message")
            reviewDate     = review.item("date")
            sMostrar       = (" - Data: " & reviewDate & " - Mensagem: " & reviewMsg)

            response.write sMostrar

            RetornoErroNetShoes = (RetornoErroNetShoes & sMostrar & vbcrlf)

            sMostrar = ""
        Next    

        oConn.execute("update TB_PRODUTOS set RetornoErroNetShoes = '"& sMostrar &"' where id = " & sku)
    end if

    oConn.execute("update TB_PRODUTOS set RetornoRetornoNetShoes = '"& sRetorno &"' where id = " & sku)

    if(sRetorno <> "") then 
        Response.Write "<br>" & sRetorno & "<br>"
    end if

End Function   
'----------------------------------

'---Fnc Exibe Detalhe do produto-----
Function getProductDetail(sku)
    'baseUrl =  "http://api-sandbox.netshoes.com.br/api/v2/products/"
    baseUrl =  "http://api-marketplace.netshoes.com.br/api/v2/products/"  

    UrlProduct   = sku&"?expands=images%2Cattributes%2Cstatus" 
    UrlProduct   = Replace(UrlProduct, ":", "%3A")
    UrlProduct   = TRIM(baseUrl&UrlProduct)

    response.write UrlProduct 
   
    Set oHttpRequest = CreateObject("MSXML2.ServerXMLHTTP")   
    with oHttpRequest    
        .Open "GET", UrlProduct    
        .SetRequestHeader "Accept", "application/json; charset=utf-8"
        .SetRequestHeader "client_id",  LoginERP
        .SetRequestHeader "access_token", SenhaERP   
        .SetRequestHeader "idseller", IdSellerNetShoes
        .Send()   
        sRetornoCode = .status
        sRetornoText = .responseText  
    End With 
    Set oHttpRequest = Nothing

    response.Write sRetornoCode & "<p></p>"
    response.write sRetornoText
End Function   
'----------------------------------


Sub MyDelay(NumberOfSeconds)
    Dim DateTimeResume
    DateTimeResume= DateAdd("s", NumberOfSeconds, Now())
    Do Until (Now() > DateTimeResume)
    Loop
End Sub

%>
