﻿<!--#include file = "../../hidden/funcoes.asp"-->
<!--#include file = "../../hidden/aspJSON1.17/aspJSON1.17.asp"-->

<%
Server.ScriptTimeOut    = 65000
Response.Buffer         = true

Response.CharSet = "UTF-8" 'editado

dim Rs, Rs1, sSql
dim sNrPedido, sNumeroObj, sEmailCliente, sCdClientePedido, sNomeCliente, sTipoServico
dim LoginERP, SenhaERP, IdSellerNetShoes

call Abre_Conexao()

 
' ===============================================================================
' MARCAR PEDIDO COM STATUS 01 
sSql = "SELECT " & _
			" P.Id,  " & _
			" P.Id_status,  " & _
            " P.cdcliente,  " & _
            " P.Rastreamento,  " & _
            " P.NotaFiscal,  " & _
            " P.ChaveNotaFiscal,  " & _
            " P.RetornoIntegracao5,  " & _
            " P.TipoEntregaTransportadora,  " & _
            " P.TipoEntregaDescricao,  " & _
            " isnull(CI.LoginIntegraCommerce, '') as LoginIntegraCommerce,  " & _
            " isnull(CI.SenhaIntegraCommerce, '') as SenhaIntegraCommerce,  " & _
            " P.CodigoServicoIG, P.RetornoIntegracao3, P.RetornoBcash  " & _
            " ,CI.LoginNetShoes, CI.SenhaNetShoes, CI.IdSellerNetShoes, CI.AccountManagerSkyHub  " & _
		" from " & _
			" TB_PEDIDOS P (NOLOCK) " & _
            " INNER JOIN TbCliente CI (NOLOCK) on p.cdcliente = CI.cdcliente " & _
		" WHERE " & _
	       " P.cdcliente in (SELECT C.CDCLIENTE FROM TbCliente C (NOLOCK) WHERE C.ATIVO = 'S') " & _
           " AND P.Id_FrmPagamento IN (91) " & _
           " AND P.Id_STATUS IN (1) " & _
           " /*AND P.ID = 3169003*/  " & _
		" order by  " & _
			" P.Id "

Set Rs = oConn.execute(sSql)
'response.write sSql
'response.end

Response.write "<font color=red>" & sSql & "</font><br><br>"
Response.flush

if(not Rs.eof) then
    sConteudoJson = ""
    
	do while not Rs.eof
        LoginERP            = trim(rs("LoginNetShoes"))
        SenhaERP            = trim(rs("SenhaNetShoes"))
        IdSellerNetShoes    = trim(rs("IdSellerNetShoes"))
        AccountManagerSkyHub= trim(rs("AccountManagerSkyHub"))
		sNrPedido 		    = trim(rs("id"))
        CodigoServicoIG     = trim(rs("CodigoServicoIG"))
        RetornoIntegracao5  = trim(rs("RetornoIntegracao5"))
        id_statusP          = trim(Rs("id_status"))
        sRetorno            = ""

        if(CodigoServicoIG <> "") then 

            UrlApiProduto       = "http://api-marketplace.netshoes.com.br/api/v1/orders/"& CodigoServicoIG &"?expand=shippings"

            response.Write "<p>UrlApiProduto:"      & UrlApiProduto & "</br>"  
            response.Write "client_id: "            & LoginERP & "</br>"
            response.Write "access_token: "         & SenhaERP & "</br>"
            response.Write "IdSellerNetShoes: "     & IdSellerNetShoes & "</p>"
            Response.flush

            call MyDelay(3)

            Set oHttpRequest = Server.CreateObject("MSXML2.ServerXMLHTTP")
            with oHttpRequest 
                .Open "GET", UrlApiProduto, false
                .SetRequestHeader "client_id",      LoginERP
                .SetRequestHeader "access_token",   SenhaERP
                .SetRequestHeader "idseller",       IdSellerNetShoes
                .Send()
                sRetorno        = .responseText
                sRetornoStatus  = .status
            End With 
            Set oHttpRequest = Nothing

            if(sRetorno <> "") then 
                Set oJSON = New aspJSON
                oJSON.loadJSON(sRetorno)

                OrderStatus     = oJSON.data("orderStatus")
                Id_Status       = DeParaStatus(OrderStatus)

                response.write "OrderStatus:" & OrderStatus & " - "& Id_Status &"<br/>"
                Response.flush

                if(id_statusP = "1") then 
            
                    if(id_statusP <> trim(Id_Status)) then 

                        sSQl = "UPDATE Tb_Pedidos SET ID_STATUS = "& Id_Status &" WHERE ID = " & sNrPedido
                        response.write sSql & "<br/>"
                        Response.flush
                        oConn.execute(sSQl)

                        if(Id_Status <> 1) then 
	                        sSQl = "INSERT INTO TB_HISTORICOS (Id_Pedido, Id_Status, Descricao) VALUES ('"& sNrPedido &"', "& Id_Status &", '"& TRIM(Recupera_Campos_Db_oConn("TB_STATUS", Id_Status, "id", "DESCRICAO")) &" [Robo NetShoes Automatico]')"
	                        oConn.execute(sSQl)

                            oConn.execute("Update TB_PEDIDOS set ID_STATUS = "& Id_Status &" where id = " & sNrPedido)

                            if(Id_Status = "2") then 
                                shippingCode    = oJSON.data("shippings").item(0).item("shippingCode")
                            end if

                            if(shippingCode <> "" and Id_Status = "2") then 
                                oConn.execute("Update TB_PEDIDOS set RetornoIntegracao5 = '"& shippingCode &"' where id = " & sNrPedido)
                            end if

                            response.write sSql & "<br/>"
                            Response.flush
                        end if

                    else
                        response.write "Status fora do fluxo (1): "& code &" - "& ID &" - "& id_statusP &" - "& Id_Status &"<br/>"
                        Response.flush
                    end if

                else
                    response.write "Status fora do fluxo (2): "& code &" - "& ID &" - "& id_statusP &" - "& Id_Status &"<br/>"
                    Response.flush
                end if
                Set oJSON = nothing
            else    
	            ' =======================================
	            Response.write "Sem sRetorno ["& sRetorno &"]: "& UrlApiProduto &"<br>"
	            Response.flush
	            ' =======================================                 
            end if
        else
	        ' =======================================
	        Response.write "Erro na URL ["& sNrPedido &"]: "& UrlApiProduto &"<br>"
	        Response.flush
	        ' ======================================= 
        end if

        Response.write "<hr>"
        Response.flush

        call MyDelay(2)
	Rs.movenext
	loop
else
	' =======================================
	Response.write "** sem pedidos status 01...<br><br><br>"
	Response.flush
	' =======================================
end if
Set Rs = nothing


' =======================================================================
function DeParaStatus(prlabelS)

    if(prlabelS = "" or prlabelS = "null") then 
        DeParaStatus = "1"
        exit function
    end if

    select case trim(ucase(prlabelS))
        case "CREATED"
            DeParaStatus = "1"
            exit function
        case "APPROVED"
            DeParaStatus = "2"
            exit function
        case "DELIVERED"
            DeParaStatus = "4"
            exit function
        case "CANCELED"
            DeParaStatus = "5"
            exit function
        case "INVOICED"
            DeParaStatus = "56"
            exit function
        case "SHIPPED"
            DeParaStatus = "6"
            exit function
    end select 
end function 

' =======================================================================================================
Sub MyDelay(NumberOfSeconds)
    Dim DateTimeResume
    DateTimeResume= DateAdd("s", NumberOfSeconds, Now())
    Do Until (Now() > DateTimeResume)
    Loop
End Sub


%>