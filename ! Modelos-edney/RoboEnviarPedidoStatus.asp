﻿<!--#include file = "../../hidden/funcoes.asp"-->
<!--#include file = "../../hidden/aspJSON1.17/aspJSON1.17.asp"-->

<%
Server.ScriptTimeOut    = 65000
Response.Buffer         = true

Response.CharSet = "UTF-8" 'editado

dim Rs, Rs1, sSql
dim sNrPedido, sNumeroObj, sEmailCliente, sCdClientePedido, sNomeCliente, sTipoServico
dim LoginERP, SenhaERP, IdSellerNetShoes, orderStatusNET

call Abre_Conexao()


'JSONTESTE = "{""expands"": [""shippings"",""items"",""devolutionItems""],""agreedDate"": 1542765600000,""paymentDate"": 1541781291000,""orderDate"": 1541781241050,""orderNumber"": ""14616897"",""totalQuantity"": 1,""originSite"": ""NETSHOES"",""orderStatus"": ""Shipped"",""orderType"": ""Sale"",""totalGross"": 448.9,""totalCommission"": 0,""totalDiscount"": 0,""totalFreight"": 89.9,""totalNet"": 359,""shippings"": [{""expands"": [""items"",""devolutionItems""],""shippingCode"": 774567944,""status"": ""Shipped"",""shippingEstimate"": 1543456800000,""deliveryTime"": 12,""freightAmount"": 89.9,""country"": ""BR"",""transport"": {""expands"": [],""trackingNumber"": ""NOTA FISCAL 60785"",""trackingLink"": ""https://servicos.rapidaocometa.com.br/rastreamento/tracking/open-tracking/"",""trackingShipDate"": 1542824895533,""shipDate"": 1542652095000,""deliveryService"": ""Normal"",""deliveryId"": ""52"",""carrier"": ""Total Express"",""carrierId"": ""793""},""invoice"": {""expands"": [],""accessKey"": ""43181102272006000143550050000607851981667302"",""date"": 1542640424000,""shipDate"": 1542643246223},""customer"": {""expands"": [],""document"": ""78949629453"",""stateInscription"": """",""customerName"": ""Simone Fernandes"",""recipientName"": ""Simone"",""tradeName"": """",""cellPhone"": ""(83)991051969"",""landLine"": ""(83)93207898"",""address"": {""expands"": [],""neighborhood"": ""cazuzinha"",""postalCode"": ""58380000"",""city"": ""INGA"",""state"": ""PB"",""stateName"": ""PARAIBA"",""street"": ""Rua emidio cardoso de almeida"",""number"": ""03"",""reference"": ""proximo a igreja jesus ressuscitado""}},""sender"": {""expands"": [],""supplierCnpj"": ""24873314000167"",""sellerCode"": 7563,""sellerName"": ""Dream Fitness"",""supplierName"": ""Dream Fitness""},""platformId"": ""INTERNAL"",""serviceId"": ""52"",""serviceName"": ""Normal"",""links"": {""self"": {""href"": ""http://prd-vs-mp.netshoes.local:8684/v1/orders/14616897/shippings/774567944""},""order"": {""href"": ""http://prd-vs-mp.netshoes.local:8684/v1/orders/14616897""}}}],""links"": {""self"": {""href"": ""http://prd-vs-mp.netshoes.local:8684/v1/orders/14616897""},""shippings"": {""href"": ""http://prd-vs-mp.netshoes.local:8684/v1/orders/14616897/shippings""}}}"

'Set oJSONLista2 = New aspJSON
'oJSONLista2.loadJSON(replace(JSONTESTE,".","#"))
'orderStatus     = oJSONLista2.data("orderStatus")
'shippingCode    = oJSONLista2.data("shippings").item(0).item("shippingCode")
'Set oJSONLista2 = nothing

'RESPONSE.WRITE orderStatus & "<BR>"
'RESPONSE.WRITE shippingCode & "<BR>"
'RESPONSE.End

 
' ===============================================================================
' MARCAR PEDIDO PARA FATURADO
sSql = "SELECT " & _
			" P.Id,  " & _
			" P.Id_status,  " & _
            " P.cdcliente,  " & _
            " P.Rastreamento,  " & _
            " P.NotaFiscal,  " & _
            " P.ChaveNotaFiscal,  " & _
            " P.RetornoIntegracao5,  " & _
            " P.TipoEntregaTransportadora,  " & _
            " P.TipoEntregaDescricao,  " & _
            " isnull(CI.LoginIntegraCommerce, '') as LoginIntegraCommerce,  " & _
            " isnull(CI.SenhaIntegraCommerce, '') as SenhaIntegraCommerce,  " & _
            " P.CodigoServicoIG, P.RetornoIntegracao3, P.RetornoBcash  " & _
            " ,CI.LoginNetShoes, CI.SenhaNetShoes, CI.IdSellerNetShoes,  " & _
            " p.DataFaturamento,  " & _
            " (SELECT top 1 H.DATACAD FROM TB_HISTORICOS	H (NOLOCK) WHERE H.ID_PEDIDO = P.ID AND H.ID_STATUS IN (56) ORDER BY DATACAD) AS DATASTATUS " & _
		" from " & _
			" TB_PEDIDOS P (NOLOCK) " & _
            " INNER JOIN TbCliente CI (NOLOCK) on p.cdcliente = CI.cdcliente " & _
		" WHERE " & _
	       " P.cdcliente in (SELECT C.CDCLIENTE FROM TbCliente C (NOLOCK) WHERE C.ATIVO = 'S') " & _
           " AND P.Id_FrmPagamento IN (91) " & _
           " AND P.Id_STATUS NOT IN (5) " & _
           " AND EXISTS (SELECT top 1 H.ID FROM TB_HISTORICOS	H (NOLOCK) WHERE H.ID_PEDIDO = P.ID AND H.ID_STATUS IN (56))" & _
           " AND isnull(P.DataStatus06,'')          = ''  " & _
           " AND isnull(P.DataStatus56,'')          = ''  " & _
           " AND isnull(P.DataStatus04,'')          = ''  " & _
           " AND isnull(P.NotaFiscal,'')            <> ''  " & _
           " AND isnull(P.ChaveNotaFiscal,'')       <> ''  " & _
           " AND isnull(CI.LoginNetShoes,'')        <> ''  " & _  
           " AND isnull(CI.SenhaNetShoes,'')        <> ''  " & _  
           " AND isnull(P.CodigoServicoIG,'')       <> ''  " & _  
		" order by  " & _
			" P.Id "

' " AND P.ID = 3168503 " & _
'response.write sSql
'response.end
Set Rs = oConn.execute(sSql)

Response.write "<font color=red>" & sSql & "</font><br><br>"
Response.flush

if(not Rs.eof) then
    sConteudoJson = ""
    
	do while not Rs.eof
		sNrPedido 		    = trim(rs("id"))
        LoginERP            = trim(rs("LoginNetShoes"))
        SenhaERP            = trim(rs("SenhaNetShoes"))
        IdSellerNetShoes    = trim(rs("IdSellerNetShoes"))

        CodigoServicoIG     = trim(rs("CodigoServicoIG"))
        ChaveNotaFiscal     = trim(rs("ChaveNotaFiscal"))
        NotaFiscal          = trim(rs("NotaFiscal"))
        RetornoIntegracao5  = trim(rs("RetornoIntegracao5"))
        DATASTATUS          = trim(rs("DataFaturamento"))
        
        if(DATASTATUS = "" or isnull(DATASTATUS)) then 
            DATASTATUS = (trim(rs("DATASTATUS")) & " 23:59:59")
        else
            DATASTATUS = (DATASTATUS & " 23:59:59")
        end if
       
        dataImput =  "{" & _
                        """danfeXml"": ""string""," & _
                        """expands"": [" & _
                        """string""" & _
                        "]," & _
                        """issueDate"": """& year(cdate(DATASTATUS)) &"-"& Right("00" & month(cdate(DATASTATUS)),2) &"-"& Right("00" & day(cdate(DATASTATUS)),2) &"T"& Right("00" & Hour(cdate(DATASTATUS)),2) &":"& Right("00" & Minute(cdate(DATASTATUS)),2) &":"& Right("00" & Second(cdate(DATASTATUS)),2) &".964Z""," & _
                        """key"": """& ChaveNotaFiscal &"""," & _
                        """line"": ""string""," & _
                        """links"": [ {" & _
                            """href"": ""string""," & _
                            """rel"": ""string""," & _
                            """templated"": ""true"" } ]," & _ 
                        """number"": """& NotaFiscal &"""," & _
                        """status"": ""Invoiced""}" 

        sUrlPRocessar = TRIM(UrlApiProduto1(CodigoServicoIG, RetornoIntegracao5, "invoiced"))
 
        if(sUrlPRocessar <> "") then 
            call getStatus(CodigoServicoIG)  
            call AtualizaShippingCode(CodigoServicoIG, sNrPedido)

            Set http = Server.CreateObject("WinHttp.WinHttpRequest.5.1")
            With http
                .Open "PUT", sUrlPRocessar , False
                .SetRequestHeader "Accept",         "application/json"
                .SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
                .SetRequestHeader "client_id",      LoginERP
                .SetRequestHeader "access_token",   SenhaERP
                .SetRequestHeader "idseller",       IdSellerNetShoes
                .Send(dataImput)
            End With
            sStatusPUT      = http.Status
            responseText    = http.responseText
            Set http = nothing

            If sStatusPUT <> "200" and sStatusPUT <> "201" Then        	   
                Set http = Server.CreateObject("WinHttp.WinHttpRequest.5.1")
                With http
                    .Open "PUT", sUrlPRocessar , False
                    .SetRequestHeader "Accept",         "application/json"
                    .SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
                    .SetRequestHeader "client_id",      LoginERP
                    .SetRequestHeader "access_token",   "ggzzyrKfjzHm"
                    .SetRequestHeader "idseller",       IdSellerNetShoes
                    .Send(dataImput)
                End With     
                sStatusPUT      = http.Status
                responseText    = http.responseText
                Set http = nothing
            end if

            If sStatusPUT = "200" OR sStatusPUT = "201" Then        	   
                response.write "<p>(1) Pedido ["& sNrPedido &", "& CodigoServicoIG &", "& rs("Id_Status") &", "& ucase(statusNetPedido) &"] atualizado (56): "& dataImput & "</p>"
                Response.flush

                oConn.execute("Update TB_PEDIDOS set DataStatus56 = getdate() where id = " & sNrPedido)
            else   

                select case trim(ucase(orderStatusNET))
                    case "INVOICED"
                        oConn.execute("Update TB_PEDIDOS set DataStatus56 = getdate() where id = " & sNrPedido)

                        response.write "<p>(2) Pedido ["& sNrPedido &", "& CodigoServicoIG &", "& rs("Id_Status") &", "& ucase(statusNetPedido) &"] atualizado (56): "& dataImput & "</p>"
                        Response.flush

                    case "DELIVERED"
                        oConn.execute("Update TB_PEDIDOS set DataStatus56 = getdate() where id = " & sNrPedido)

                        sConteudoEmail = "Pedido ADMIN: "& sNrPedido & "<br>"
                        sConteudoEmail = sConteudoEmail & "Status ADMIN: "& rs("id_status") & "<br>"
                        sConteudoEmail = sConteudoEmail & "Pedido NETSHOES: "& CodigoServicoIG & "<br>"
                        sConteudoEmail = sConteudoEmail & "Status NETSHOES: "& trim(ucase(orderStatusNET)) & "<br>"

                        call Envia_Email("edney@ominic.com.br",         "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Entregue NetShoes e Faturado ADMIN", sConteudoEmail)
                        call Envia_Email("juliana.ramos@ominic.com.br", "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Entregue NetShoes e Faturado ADMIN", sConteudoEmail)
                        call Envia_Email("vanilde.reis@ominic.com.br",  "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Entregue NetShoes e Faturado ADMIN", sConteudoEmail)

                        response.write "<p>(3) ****<font color=red>Pedido ["& sNrPedido &"] não atualizado:<br/>sUrlPRocessar:"& sUrlPRocessar & _ 
                                                "<br/>dataImput:        "& dataImput & _ 
                                                "<br/>http.Status:      "& http.Status & _ 
                                                "<br/>http.ResponseText:"& http.ResponseText & _
                                                "<br/>LoginERP:         "& LoginERP & _
                                                "<br/>SenhaERP:         "& SenhaERP & _
                                                "<br/>IdSellerNetShoes: "& IdSellerNetShoes & _
                                        "<br/></font></p>"
                        Response.flush

                    case "CANCELED"
                        oConn.execute("Update TB_PEDIDOS set DataStatus56 = getdate() where id = " & sNrPedido)

                        sConteudoEmail = "Pedido ADMIN: "& sNrPedido & "<br>"
                        sConteudoEmail = sConteudoEmail & "Status ADMIN: "& rs("id_status") & "<br>"
                        sConteudoEmail = sConteudoEmail & "Pedido NETSHOES: "& CodigoServicoIG & "<br>"
                        sConteudoEmail = sConteudoEmail & "Status NETSHOES: "& ucase(orderStatusNET) & "<br>"

                        call Envia_Email("edney@ominic.com.br",         "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Cancelado NetShoes e Faturado ADMIN", sConteudoEmail)
                        call Envia_Email("juliana.ramos@ominic.com.br", "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Cancelado NetShoes e Faturado ADMIN", sConteudoEmail)
                        call Envia_Email("vanilde.reis@ominic.com.br",  "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Cancelado NetShoes e Faturado ADMIN", sConteudoEmail)

                        response.write "<p>(4) ****<font color=red>Pedido ["& sNrPedido &"] não atualizado:<br/>sUrlPRocessar:"& sUrlPRocessar & _ 
                                                "<br/>dataImput:        "& dataImput & _ 
                                                "<br/>http.Status:      "& http.Status & _ 
                                                "<br/>http.ResponseText:"& http.ResponseText & _
                                                "<br/>LoginERP:         "& LoginERP & _
                                                "<br/>SenhaERP:         "& SenhaERP & _
                                                "<br/>IdSellerNetShoes: "& IdSellerNetShoes & _
                                        "<br/></font></p>"
                        Response.flush

                    case "SHIPPED"
                        oConn.execute("Update TB_PEDIDOS set DataStatus56 = getdate() where id = " & sNrPedido)

                        sConteudoEmail = "Pedido ADMIN: "& sNrPedido & "<br>"
                        sConteudoEmail = sConteudoEmail & "Status ADMIN: "& rs("id_status") & "<br>"
                        sConteudoEmail = sConteudoEmail & "Pedido NETSHOES: "& CodigoServicoIG & "<br>"
                        sConteudoEmail = sConteudoEmail & "Status NETSHOES: "& trim(ucase(orderStatusNET)) & "<br>"

                        call Envia_Email("edney@ominic.com.br",         "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Enviado NetShoes e Faturado ADMIN", sConteudoEmail)
                        call Envia_Email("juliana.ramos@ominic.com.br", "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Enviado NetShoes e Faturado ADMIN", sConteudoEmail)
                        call Envia_Email("vanilde.reis@ominic.com.br",  "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Enviado NetShoes e Faturado ADMIN", sConteudoEmail)

                        response.write "<p>(5) ****<font color=red>Pedido ["& sNrPedido &"] não atualizado:<br/>sUrlPRocessar:"& sUrlPRocessar & _ 
                                                "<br/>dataImput:        "& dataImput & _ 
                                                "<br/>http.Status:      "& http.Status & _ 
                                                "<br/>http.ResponseText:"& http.ResponseText & _
                                                "<br/>LoginERP:         "& LoginERP & _
                                                "<br/>SenhaERP:         "& SenhaERP & _
                                                "<br/>IdSellerNetShoes: "& IdSellerNetShoes & _
                                        "<br/></font></p>"
                        Response.flush

                    case else
                        response.write "<p>(6)****<font color=red>Pedido [ "& sNrPedido &"] não atualizado:" & _ 
                                                "<br/>sUrlPRocessar:    "& sUrlPRocessar & _ 
                                                "<br/>dataImput:        "& dataImput & _ 
                                                "<br/>responseText:     "& responseText & _ 
                                                "<br/>LoginERP:         "& LoginERP & _
                                                "<br/>SenhaERP:         "& SenhaERP & _
                                                "<br/>IdSellerNetShoes: "& IdSellerNetShoes & _
                                                "<br/>orderStatusNET:   "& trim(ucase(orderStatusNET)) & _ 
                                        "<br/></font></p>"
                        Response.flush                    


                end select 

            end if
        else
	        ' =======================================
	        Response.write "Erro na URL ["& sNrPedido &"]: "& sUrlPRocessar &"<br>"
	        Response.flush
	        ' ======================================= 
        end if

        call MyDelay(2)

	Rs.movenext
	loop
else
	' =======================================
	Response.write "** sem pedidos status 56...<br><br><br>"
	Response.flush
	' =======================================
end if
Set Rs = nothing

' =======================================
'RESPONSE.End
Response.write "<hr>"
Response.flush
' =======================================


' ===============================================================================
' MARCAR PEDIDO PARA ENVIADO
sSql = "SELECT " & _
			" P.Id,  " & _
            " P.datacad,  " & _
			" P.Id_status,  " & _
            " P.cdcliente,  " & _
            " P.Rastreamento,  " & _
            " P.ChaveNotaFiscal,  " & _
            " P.TipoEntregaTransportadora,  " & _
            " P.TipoEntregaDescricao,  " & _
            " P.RetornoIntegracao5,  " & _
            " isnull(CI.LoginIntegraCommerce, '') as LoginIntegraCommerce,  " & _
            " isnull(CI.SenhaIntegraCommerce, '') as SenhaIntegraCommerce,  " & _
            " P.CodigoServicoIG, P.RetornoBcash, P.NotaFiscal  " & _
            " ,CI.LoginNetShoes, CI.SenhaNetShoes, CI.IdSellerNetShoes  " & _
            " ,(SELECT top 1 H.DATACAD FROM TB_HISTORICOS	H (NOLOCK) WHERE H.ID_PEDIDO = P.ID AND H.ID_STATUS IN (6) ORDER BY DATACAD) AS DATASTATUS " & _
		" from " & _
			" TB_PEDIDOS P (NOLOCK) " & _
            " INNER JOIN TbCliente CI (NOLOCK) on p.cdcliente = CI.cdcliente " & _
		" WHERE 1=1 " & _
	       " AND P.cdcliente in (SELECT C.CDCLIENTE FROM TbCliente C (NOLOCK) WHERE C.ATIVO = 'S') " & _
           " AND P.Id_FrmPagamento IN (91) " & _
           " AND P.Id_STATUS NOT IN (4, 5) " & _
           " AND EXISTS (SELECT top 1 H.ID FROM TB_HISTORICOS	H (NOLOCK) WHERE H.ID_PEDIDO = P.ID AND H.ID_STATUS IN (6)) " & _
           " AND isnull(P.DataStatus06,'')      = ''  " & _
           " AND isnull(P.DataStatus04,'')      = ''  " & _
           " AND isnull(P.DataStatus56,'')      <> ''  " & _
           " AND isnull(P.Rastreamento,'')     <> ''  " & _
           " AND isnull(P.ChaveNotaFiscal,'')   <> ''  " & _
           " AND isnull(CI.LoginNetShoes,'')   <> ''  " & _  
           " AND isnull(CI.SenhaNetShoes,'')    <> ''  " & _  
           " AND isnull(P.CodigoServicoIG,'')   <> ''  " & _  
		" order by  " & _
			" P.Id "

'" AND P.ID = 3168598 " & _
'response.Write sSql
'response.end
Set Rs = oConn.execute(sSql)            
        
Response.write "<font color=red>" & sSql & "</font><br><br>"
Response.flush

if(not Rs.eof) then
    sConteudoJson = ""

	do while not Rs.eof
        LoginERP                    = trim(rs("LoginNetShoes"))
        SenhaERP                    = trim(rs("SenhaNetShoes"))
        IdSellerNetShoes            = trim(rs("IdSellerNetShoes"))
		sNrPedido 		            = trim(rs("id"))
        CodigoServicoIG             = trim(rs("CodigoServicoIG"))
        Rastreamento                = trim(rs("Rastreamento"))
        NotaFiscal                  = trim(rs("NotaFiscal"))
        ChaveNotaFiscal             = trim(rs("ChaveNotaFiscal"))
        TipoEntregaTransportadora   = trim(rs("TipoEntregaTransportadora"))
        TipoEntregaDescricao        = trim(rs("TipoEntregaDescricao"))
        RetornoBcash                = Rs("RetornoBcash")
        RetornoIntegracao5          = trim(rs("RetornoIntegracao5"))
        DATASTATUS                  = trim(rs("DATASTATUS"))
        

        Rastreamento                = replace(replace(Rastreamento, "	", " "), vbcrlf, " ")

        dataImput =  "{" & _
                        """carrier"": """& left(TipoEntregaTransportadora, 50) &"""," & _
                        """deliveredCarrierDate"":  """& year(cdate(DATASTATUS)) &"-"& Right("00" & month(cdate(DATASTATUS)),2) &"-"& Right("00" & day(cdate(DATASTATUS)),2) &"T"& Right("00" & Hour(cdate(DATASTATUS)),2) &":"& Right("00" & Minute(cdate(DATASTATUS)),2) &":"& Right("00" & Second(cdate(DATASTATUS)),2) &".964Z""," & _
                        """estimatedDelivery"":     """& year(cdate(DATASTATUS)+ 20) &"-"& Right("00" & month(cdate(DATASTATUS)+ 20),2) &"-"& Right("00" & day(cdate(DATASTATUS)+ 20),2) &"T"& Right("00" & Hour(cdate(DATASTATUS)),2) &":"& Right("00" & Minute(cdate(DATASTATUS)),2) &":"& Right("00" & Second(cdate(DATASTATUS)),2) &".964Z""," & _
                        """expands"": [" & _
                            """"& Rastreamento &"""" & _
                        "]," & _   
                        """links"": [ {" & _
                            """href"": ""string""," & _
                            """rel"": ""string""," & _
                            """templated"": ""true"" } ]," & _ 
                        """status"": ""shipped""," & _
                        """trackingLink"": """& TipoEntregaDescricao &"""," & _
                        """trackingNumber"": """& Rastreamento &"""" &_
                    "}"                                 

        sUrlPRocessar = TRIM(UrlApiProduto1(CodigoServicoIG, RetornoIntegracao5, "shipped"))
                       
        if(sUrlPRocessar <> "") then 
            call getStatus(CodigoServicoIG)  
            call AtualizaShippingCode(CodigoServicoIG, sNrPedido)

            Set http = Server.CreateObject("WinHttp.WinHttpRequest.5.1")
            With http
                .Open "PUT", sUrlPRocessar, False
                .SetRequestHeader "Accept",   "application/json"
                .SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
                .SetRequestHeader "client_id", LoginERP
                .SetRequestHeader "access_token", SenhaERP
                .SetRequestHeader "idseller", IdSellerNetShoes
                .Send(dataImput)
            End With
            sStatusPUT      = http.Status
            responseText    = http.responseText
            Set http = nothing

            If sStatusPUT <> "200" and sStatusPUT <> "201" Then        	   
                Set http = Server.CreateObject("WinHttp.WinHttpRequest.5.1")
                With http
                    .Open "PUT", sUrlPRocessar , False
                    .SetRequestHeader "Accept",         "application/json"
                    .SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
                    .SetRequestHeader "client_id",      LoginERP
                    .SetRequestHeader "access_token",   "ggzzyrKfjzHm"
                    .SetRequestHeader "idseller",       IdSellerNetShoes
                    .Send(dataImput)
                End With     
                sStatusPUT      = http.Status
                responseText    = http.responseText
                Set http = nothing
            end if

            If sStatusPUT = "200" OR sStatusPUT = "201" Then        	   
                response.write "<p>(1) Pedido ["& sNrPedido &", "& CodigoServicoIG &", "& rs("Id_Status") &", "& ucase(statusNetPedido) &"] atualizado (06): "& dataImput & "</p>"
                Response.flush

                oConn.execute("Update TB_PEDIDOS set DataStatus06 = getdate() where id = " & sNrPedido)
            else

                select case trim(ucase(orderStatusNET))
                    case "SHIPPED"
                        oConn.execute("Update TB_PEDIDOS set DataStatus06 = getdate() where id = " & sNrPedido)

                        response.write "<p>(2) Pedido ["& sNrPedido &", "& CodigoServicoIG &", "& rs("Id_Status") &", "& ucase(statusNetPedido) &"] atualizado (06): "& dataImput & "</p>"
                        Response.flush

                    case "DELIVERED"
                        oConn.execute("Update TB_PEDIDOS set DataStatus06 = getdate() where id = " & sNrPedido)

                        sConteudoEmail = "Pedido ADMIN: "& sNrPedido & "<br>"
                        sConteudoEmail = sConteudoEmail & "Status ADMIN: "& rs("id_status") & "<br>"
                        sConteudoEmail = sConteudoEmail & "Pedido NETSHOES: "& CodigoServicoIG & "<br>"
                        sConteudoEmail = sConteudoEmail & "Status NETSHOES: "& trim(ucase(orderStatusNET)) & "<br>"

                        call Envia_Email("edney@ominic.com.br",         "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Entregue NetShoes e Faturado ADMIN", sConteudoEmail)
                        call Envia_Email("juliana.ramos@ominic.com.br", "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Entregue NetShoes e Faturado ADMIN", sConteudoEmail)
                        call Envia_Email("vanilde.reis@ominic.com.br",  "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Entregue NetShoes e Faturado ADMIN", sConteudoEmail)

                        response.write "<p>(3)****<font color=red>Pedido ["& sNrPedido &"] não atualizado:<br/>sUrlPRocessar:"& sUrlPRocessar & _ 
                                                "<br/>dataImput:        "& dataImput & _ 
                                                "<br/>http.Status:      "& http.Status & _ 
                                                "<br/>http.ResponseText:"& http.ResponseText & _
                                                "<br/>LoginERP:         "& LoginERP & _
                                                "<br/>SenhaERP:         "& SenhaERP & _
                                                "<br/>IdSellerNetShoes: "& IdSellerNetShoes & _
                                        "<br/></font></p>"
                        Response.flush

                    case "CANCELED"
                        oConn.execute("Update TB_PEDIDOS set DataStatus06 = getdate() where id = " & sNrPedido)

                        sConteudoEmail = "Pedido ADMIN: "& sNrPedido & "<br>"
                        sConteudoEmail = sConteudoEmail & "Status ADMIN: "& rs("id_status") & "<br>"
                        sConteudoEmail = sConteudoEmail & "Pedido NETSHOES: "& CodigoServicoIG & "<br>"
                        sConteudoEmail = sConteudoEmail & "Status NETSHOES: "& ucase(orderStatusNET) & "<br>"

                        call Envia_Email("edney@ominic.com.br",         "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Cancelado NetShoes e Faturado ADMIN", sConteudoEmail)
                        call Envia_Email("juliana.ramos@ominic.com.br", "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Cancelado NetShoes e Faturado ADMIN", sConteudoEmail)
                        call Envia_Email("vanilde.reis@ominic.com.br",  "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Cancelado NetShoes e Faturado ADMIN", sConteudoEmail)

                        response.write "<p>(4)****<font color=red>Pedido ["& sNrPedido &"] não atualizado:<br/>sUrlPRocessar:"& sUrlPRocessar & _ 
                                                "<br/>dataImput:        "& dataImput & _ 
                                                "<br/>http.Status:      "& http.Status & _ 
                                                "<br/>http.ResponseText:"& http.ResponseText & _
                                                "<br/>LoginERP:         "& LoginERP & _
                                                "<br/>SenhaERP:         "& SenhaERP & _
                                                "<br/>IdSellerNetShoes: "& IdSellerNetShoes & _
                                        "<br/></font></p>"
                        Response.flush
    
                    case else
                        response.write "<p>(5)****<font color=red>Pedido [ "& sNrPedido &"] não atualizado:" & _ 
                                                "<br/>sUrlPRocessar:    "& sUrlPRocessar & _ 
                                                "<br/>dataImput:        "& dataImput & _ 
                                                "<br/>responseText:     "& responseText & _ 
                                                "<br/>LoginERP:         "& LoginERP & _
                                                "<br/>SenhaERP:         "& SenhaERP & _
                                                "<br/>IdSellerNetShoes: "& IdSellerNetShoes & _
                                                "<br/>orderStatusNET:   "& trim(ucase(orderStatusNET)) & _ 
                                        "<br/></font></p>"
                        Response.flush                    

                end select 
            end if

        else
	        ' =======================================
	        Response.write "Erro na URL ["& sNrPedido &"]: "& sUrlPRocessar &"<br>"
	        Response.flush
	        ' =======================================                
        end if

        call MyDelay(2)

	Rs.movenext
	loop
else
	' =======================================
	Response.write "** sem pedidos status 06...<br><br><br>"
	Response.flush
	' =======================================
end if
Set Rs = nothing

' =======================================
'RESPONSE.End
Response.write "<hr>"
Response.flush
' =======================================

    
' ===============================================================================
' MARCAR PEDIDO PARA ENTREGUE
sSql = "SELECT  " & _
			" P.Id,  " & _
			" P.Id_status,  " & _
            " P.cdcliente,  " & _
            " P.Rastreamento,  " & _
            " P.ChaveNotaFiscal,  " & _
            " P.TipoEntregaTransportadora,  " & _
            " P.TipoEntregaDescricao,  " & _
            " P.RetornoIntegracao5,  " & _
            " isnull(CI.LoginIntegraCommerce, '') as LoginIntegraCommerce,  " & _
            " isnull(CI.SenhaIntegraCommerce, '') as SenhaIntegraCommerce,  " & _
            " P.CodigoServicoIG, p.DataStatus06, p.DataStatus56, p.NotaFiscal, p.Rastreamento, p.RetornoBcash  " & _
            " ,P.DataEntrega  " & _
            " ,(SELECT top 1 H.DATACAD FROM TB_HISTORICOS	H (NOLOCK) WHERE H.ID_PEDIDO = P.ID AND H.ID_STATUS IN (4) ORDER BY DATACAD) AS DATASTATUS " & _
            " ,CI.LoginNetShoes, CI.SenhaNetShoes, CI.IdSellerNetShoes  " & _
		" from " & _
			" TB_PEDIDOS P (NOLOCK) " & _
            " INNER JOIN TbCliente CI (NOLOCK) on p.cdcliente = CI.cdcliente " & _
		" WHERE " & _
	       " P.cdcliente in (SELECT C.CDCLIENTE FROM TbCliente C (NOLOCK) WHERE C.ATIVO = 'S') " & _
           " AND P.Id_FrmPagamento IN (91) " & _
           " AND P.Id_STATUS NOT IN (5) " & _
           " AND EXISTS (SELECT top 1 H.ID FROM TB_HISTORICOS	H (NOLOCK) WHERE H.ID_PEDIDO = P.ID AND H.ID_STATUS IN (4))" & _
           " AND isnull(P.DataStatus04,'')      = ''  " & _
           " AND isnull(P.DataStatus06,'')      <> ''  " & _
           " AND isnull(P.DataStatus56,'')      <> ''  " & _
           " AND isnull(P.Rastreamento,'')      <> ''  " & _
           " AND isnull(P.ChaveNotaFiscal,'')   <> ''  " & _
           " AND isnull(CI.LoginNetShoes,'')   <> ''  " & _  
           " AND isnull(CI.SenhaNetShoes,'')   <> ''  " & _  
           " AND isnull(P.CodigoServicoIG,'')   <> ''  " & _  
		" order by  " & _
			" P.Id "

' " AND P.ID = 3168503 " & _
'response.write sSql
'response.end
Set Rs = oConn.execute(sSql)

Response.write "<font color=red>" & sSql & "</font><br><br>"
Response.flush

 
if(not Rs.eof) then
    sConteudoJson = ""

	do while not Rs.eof
        LoginERP                    = trim(rs("LoginNetShoes"))
        SenhaERP                    = trim(rs("SenhaNetShoes"))
        IdSellerNetShoes            = trim(rs("IdSellerNetShoes"))
		sNrPedido 		            = trim(rs("id"))
        CodigoServicoIG             = trim(rs("CodigoServicoIG"))
        Rastreamento                = trim(rs("Rastreamento"))
        NotaFiscal                  = trim(rs("NotaFiscal"))
        ChaveNotaFiscal             = trim(rs("ChaveNotaFiscal"))
        TipoEntregaTransportadora   = trim(rs("TipoEntregaTransportadora"))
        TipoEntregaDescricao        = trim(rs("TipoEntregaDescricao"))
        DataStatus06                = Rs("DataStatus06")
        DataStatus56                = Rs("DataStatus56")
        RetornoIntegracao5          = trim(rs("RetornoIntegracao5"))
        DataEntrega                 = trim(rs("DataEntrega"))

        if(DataEntrega = "" or IsNull(DataEntrega) ) then 
            DATASTATUS = trim(rs("DATASTATUS"))
        else
            DATASTATUS = (DataEntrega & " 23:59:59")
            'DATASTATUS = DataEntrega
        end if

        dataImput = "{" & _
                          """deliveryDate"":  """& year(cdate(DATASTATUS)) &"-"& Right("00" & month(cdate(DATASTATUS)), 2) &"-"& Right("00" & day(cdate(DATASTATUS)), 2) &"T"& Right("00" & Hour(cdate(DATASTATUS)),2) &":"& Right("00" & Minute(cdate(DATASTATUS)),2) &":"& Right("00" & Second(cdate(DATASTATUS)),2) &".964Z""," & _
                          """status"": ""delivered""" & _                          
                    "}"

        sUrlPRocessar = TRIM(UrlApiProduto1(CodigoServicoIG, RetornoIntegracao5, "delivered"))

        if(sUrlPRocessar <> "") then 
            call getStatus(CodigoServicoIG)  
            call AtualizaShippingCode(CodigoServicoIG, sNrPedido)

            Set http = Server.CreateObject("WinHttp.WinHttpRequest.5.1")
            With http
                .Open "PUT", sUrlPRocessar, False
                .SetRequestHeader "Accept",   "application/json"
                .SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
                .SetRequestHeader "client_id", LoginERP
                .SetRequestHeader "access_token", SenhaERP
                .SetRequestHeader "idseller", IdSellerNetShoes
                .Send(dataImput)
            End With
            sStatusPUT      = http.Status
            responseText    = http.responseText
            Set http = nothing

            If sStatusPUT <> "200" and sStatusPUT <> "201" Then        	   
                Set http = Server.CreateObject("WinHttp.WinHttpRequest.5.1")
                With http
                    .Open "PUT", sUrlPRocessar , False
                    .SetRequestHeader "Accept",         "application/json"
                    .SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
                    .SetRequestHeader "client_id",      LoginERP
                    .SetRequestHeader "access_token",   "ggzzyrKfjzHm"
                    .SetRequestHeader "idseller",       IdSellerNetShoes
                    .Send(dataImput)
                End With     
                sStatusPUT      = http.Status
                responseText    = http.responseText
                Set http = nothing
            end if

            If sStatusPUT = "200" OR sStatusPUT = "201" Then        	   
                response.write "<p>(1) Pedido ["& sNrPedido &", "& CodigoServicoIG &", "& rs("Id_Status") &", "& ucase(statusNetPedido) &"] atualizado (04): "& dataImput

                oConn.execute("Update TB_PEDIDOS set DataStatus04 = getdate() where id = " & sNrPedido)

                response.write "</p>"
                Response.flush
            else

                select case trim(ucase(orderStatusNET))
                    case "DELIVERED"
                        oConn.execute("Update TB_PEDIDOS set DataStatus04 = getdate() where id = " & sNrPedido)

                        response.write "<p>(2) Pedido ["& sNrPedido &", "& CodigoServicoIG &", "& rs("Id_Status") &", "& ucase(statusNetPedido) &"] atualizado (04): "& dataImput & "</p>"
                        Response.flush

                    case "APPROVED"
                        oConn.execute("Update TB_PEDIDOS set DataStatus04 = getdate() where id = " & sNrPedido)

                        sConteudoEmail = "Pedido ADMIN: "& sNrPedido & "<br>"
                        sConteudoEmail = sConteudoEmail & "Status ADMIN: "& rs("id_status") & "<br>"
                        sConteudoEmail = sConteudoEmail & "Pedido NETSHOES: "& CodigoServicoIG & "<br>"
                        sConteudoEmail = sConteudoEmail & "Status NETSHOES: "& trim(ucase(orderStatusNET)) & "<br>"

                        call Envia_Email("edney@ominic.com.br",         "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Aprovado NetShoes e Entregue ADMIN", sConteudoEmail)
                        call Envia_Email("juliana.ramos@ominic.com.br", "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Aprovado NetShoes e Entregue ADMIN", sConteudoEmail)
                        call Envia_Email("vanilde.reis@ominic.com.br",  "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Aprovado NetShoes e Entregue ADMIN", sConteudoEmail)

                        response.write "<p>(3) ****<font color=red>Pedido ["& sNrPedido &"] não atualizado:<br/>sUrlPRocessar:"& sUrlPRocessar & _ 
                                                "<br/>dataImput:        "& dataImput & _ 
                                                "<br/>http.Status:      "& http.Status & _ 
                                                "<br/>http.ResponseText:"& http.ResponseText & _
                                                "<br/>LoginERP:         "& LoginERP & _
                                                "<br/>SenhaERP:         "& SenhaERP & _
                                                "<br/>IdSellerNetShoes: "& IdSellerNetShoes & _
                                        "<br/></font></p>"
                        Response.flush

                    case "INVOICED"
                        oConn.execute("Update TB_PEDIDOS set DataStatus04 = getdate() where id = " & sNrPedido)

                        sConteudoEmail = "Pedido ADMIN: "& sNrPedido & "<br>"
                        sConteudoEmail = sConteudoEmail & "Status ADMIN: "& rs("id_status") & "<br>"
                        sConteudoEmail = sConteudoEmail & "Pedido NETSHOES: "& CodigoServicoIG & "<br>"
                        sConteudoEmail = sConteudoEmail & "Status NETSHOES: "& ucase(orderStatusNET) & "<br>"

                        call Envia_Email("edney@ominic.com.br",         "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Faturado NetShoes e Entregue ADMIN", sConteudoEmail)
                        call Envia_Email("juliana.ramos@ominic.com.br", "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Faturado NetShoes e Entregue ADMIN", sConteudoEmail)
                        call Envia_Email("vanilde.reis@ominic.com.br",  "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Faturado NetShoes e Entregue ADMIN", sConteudoEmail)

                        response.write "<p>(4) ****<font color=red>Pedido ["& sNrPedido &"] não atualizado:<br/>sUrlPRocessar:"& sUrlPRocessar & _ 
                                                "<br/>dataImput:        "& dataImput & _ 
                                                "<br/>http.Status:      "& http.Status & _ 
                                                "<br/>http.ResponseText:"& http.ResponseText & _
                                                "<br/>LoginERP:         "& LoginERP & _
                                                "<br/>SenhaERP:         "& SenhaERP & _
                                                "<br/>IdSellerNetShoes: "& IdSellerNetShoes & _
                                        "<br/></font></p>"
                        Response.flush

                    case "CANCELED"
                        oConn.execute("Update TB_PEDIDOS set DataStatus04 = getdate() where id = " & sNrPedido)

                        sConteudoEmail = "Pedido ADMIN: "& sNrPedido & "<br>"
                        sConteudoEmail = sConteudoEmail & "Status ADMIN: "& rs("id_status") & "<br>"
                        sConteudoEmail = sConteudoEmail & "Pedido NETSHOES: "& CodigoServicoIG & "<br>"
                        sConteudoEmail = sConteudoEmail & "Status NETSHOES: "& ucase(orderStatusNET) & "<br>"

                        call Envia_Email("edney@ominic.com.br",         "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Cancelado NetShoes e Entregue ADMIN", sConteudoEmail)
                        call Envia_Email("juliana.ramos@ominic.com.br", "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Cancelado NetShoes e Entregue ADMIN", sConteudoEmail)
                        call Envia_Email("vanilde.reis@ominic.com.br",  "sistema01@ominic.com.br", "Robo Admin - NetShoes","AVISO PERIGO - Pedido Cancelado NetShoes e Entregue ADMIN", sConteudoEmail)

                        response.write "<p>(5) ****<font color=red>Pedido ["& sNrPedido &"] não atualizado:<br/>sUrlPRocessar:"& sUrlPRocessar & _ 
                                                "<br/>dataImput:        "& dataImput & _ 
                                                "<br/>http.Status:      "& http.Status & _ 
                                                "<br/>http.ResponseText:"& http.ResponseText & _
                                                "<br/>LoginERP:         "& LoginERP & _
                                                "<br/>SenhaERP:         "& SenhaERP & _
                                                "<br/>IdSellerNetShoes: "& IdSellerNetShoes & _
                                        "<br/></font></p>"
                        Response.flush

                    case else
                        response.write "<p>(6)****<font color=red>Pedido [ "& sNrPedido &"] não atualizado:" & _ 
                                                "<br/>sUrlPRocessar:    "& sUrlPRocessar & _ 
                                                "<br/>dataImput:        "& dataImput & _ 
                                                "<br/>responseText:     "& responseText & _ 
                                                "<br/>LoginERP:         "& LoginERP & _
                                                "<br/>SenhaERP:         "& SenhaERP & _
                                                "<br/>IdSellerNetShoes: "& IdSellerNetShoes & _
                                                "<br/>orderStatusNET:   "& trim(ucase(orderStatusNET)) & _ 
                                        "<br/></font></p>"
                        Response.flush                    

                end select 
            end if


        else
	        ' =======================================
	        Response.write "Erro na URL ["& sNrPedido &"]: "& sUrlPRocessar &"<br>"
	        Response.flush
	        ' ======================================= 
        end if

        call MyDelay(2)

	Rs.movenext
	loop
else
	' =======================================
	Response.write "** sem pedidos status 04...<br><br><br>"
	Response.flush
	' =======================================
end if
Set Rs = nothing

call Fecha_Conexao()





' =======================================================================================================
Function RandNo()
    Randomize
    RandNo = Int(99999 * Rnd + 3)
End Function

' =======================================================================================================
Sub MyDelay(NumberOfSeconds)
    Dim DateTimeResume
    DateTimeResume= DateAdd("s", NumberOfSeconds, Now())
    Do Until (Now() > DateTimeResume)
    Loop
End Sub

' =======================================================================================================
Function UrlApiProduto1(numeroPedido, shippingCode, statusPedido)
    UrlApiProduto1 = ""
    
    if( (numeroPedido <> "" and not isnull(numeroPedido)) and (shippingCode <> "" and not isnull(shippingCode)) and (statusPedido <> "" and not isnull(statusPedido)) ) then 
        UrlApiProduto1  = "http://api-marketplace.netshoes.com.br/api/v1/orders/" & numeroPedido &"/shippings/"& shippingCode &"/status/"& statusPedido
    end if
End Function


' =======================================================================================================
Sub AtualizaShippingCode(sku, prsNrPedido)
    dim sRetornoStatus, url, oHttpRequest, oJSONLista, shippingCode

    url = "http://api-marketplace.netshoes.com.br/api/v1/orders/"& sku &"?expand=shippings"

    call MyDelay(2)

    On Error Resume Next

    Set oHttpRequest = CreateObject("MSXML2.ServerXMLHTTP")  
    with oHttpRequest    
        .Open "GET", url    
        .SetRequestHeader "Accept", "application/json; charset=utf-8"
        .SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
        .SetRequestHeader "client_id",      LoginERP
        .SetRequestHeader "access_token",   SenhaERP
        .SetRequestHeader "idseller",       IdSellerNetShoes
        .Send()  
        sRetornoCode = .status
        sRetornoStatus = .responseText  
    End With 
    Set oHttpRequest = Nothing   

    if(Err.number <> 0) then 
        response.write " - <font color=red>Erro na busca do DADOS PEDIDO: ["& sku &"]  - "& Err.Description & "</font>"
        Response.flush
        on error goto 0
    else

        if(sRetornoStatus <> "") then 

            Set oJSONLista = New aspJSON
            oJSONLista.loadJSON(replace(sRetornoStatus,".","#")) 
            shippingCode   = oJSONLista.data("shippings").item(0).item("shippingCode")
            Set oJSONLista = Nothing

            if(shippingCode <> "") then 
                oConn.execute("Update TB_PEDIDOS set RetornoIntegracao5 = '"& shippingCode &"' where id = " & prsNrPedido)

                'response.write " - shippingCode Atualizado: ["& sku &", "& shippingCode &"]"
                'Response.flush
            end if
        end if

    end if

End Sub

' =======================================================================================================
Sub getStatus(sku)
    dim sRetornoStatus, url, oHttpRequest, oJSONLista

    orderStatusNET = ""

    url     = "http://api-marketplace.netshoes.com.br/api/v1/orders/" & sku

    call MyDelay(2)

    On Error Resume Next

    Set oHttpRequest = CreateObject("MSXML2.ServerXMLHTTP")  
    with oHttpRequest    
        .Open "GET", url    
        .SetRequestHeader "Accept", "application/json; charset=utf-8"
        .SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
        .SetRequestHeader "client_id",      LoginERP
        .SetRequestHeader "access_token",   SenhaERP
        .SetRequestHeader "idseller",       IdSellerNetShoes
        .Send()  
        sRetornoCode = .status
        sRetornoStatus = .responseText  
    End With 
    Set oHttpRequest = Nothing   

    If sRetornoCode <> "200" and sRetornoCode <> "201" Then        	   
        Set oHttpRequest = CreateObject("MSXML2.ServerXMLHTTP")  
        with oHttpRequest    
            .Open "GET", url    
            .SetRequestHeader "Accept", "application/json; charset=utf-8"
            .SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
            .SetRequestHeader "client_id",      LoginERP
            .SetRequestHeader "access_token",   "ggzzyrKfjzHm"
            .SetRequestHeader "idseller",       IdSellerNetShoes
            .Send()  
            sRetornoCode = .status
            sRetornoStatus = .responseText  
        End With 
        Set oHttpRequest = Nothing   
    end if

    'response.Write "sRetornoStatus: " & sRetornoStatus
    'response.End

    if(Err.number <> 0) then 
        response.write " - <font color=red>Erro na busca do STATUS: ["& sku &"]  - "& Err.Description & "</font>"
        Response.flush
        on error goto 0
    else
        if(sRetornoStatus <> "") then 
            Set oJSONLista = New aspJSON
            oJSONLista.loadJSON(replace(sRetornoStatus,".","#")) 
            orderStatusNET = oJSONLista.data("orderStatus")
            Set oJSONLista = Nothing
        end if
    end if


End Sub
    
' =======================================================================================================
function DeParaStatus(prlabelS)

    if(prlabelS = "" or prlabelS = "null") then 
        DeParaStatus = "1"
        exit function
    end if

    select case trim(ucase(prlabelS))
        case "CREATED"
            DeParaStatus = "1"
            exit function
        case "APPROVED"
            DeParaStatus = "2"
            exit function
        case "DELIVERED"
            DeParaStatus = "4"
            exit function
        case "CANCELED"
            DeParaStatus = "5"
            exit function
        case "INVOICED"
            DeParaStatus = "56"
            exit function
        case "SHIPPED"
            DeParaStatus = "6"
            exit function
    end select 
end function 


%>