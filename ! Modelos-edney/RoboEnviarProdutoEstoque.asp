﻿<!--#include file = "../../hidden/funcoes.asp"-->
<%
Server.ScriptTimeOut    = 65000
Response.Buffer         = true

     Response.CharSet = "UTF-8" 'editado

dim Rs, Rs1, sSql
dim sNrPedido, sNumeroObj, sEmailCliente, sCdClientePedido, sNomeCliente, sTipoServico
dim LoginERP, SenhaERP, UrlApiProduto0, IdSellerNetShoes

Chave               = Replace_Caracteres_Especiais(Request("chave"))
    
if(Chave = "") then 
    response.write "[ERRO(1)]"
    response.End
end if
  
call Abre_Conexao()
     
  
' ===============================================================================
sSql =  "SELECT top 1 Id, Qte_Estoque, TBCLIENTE.LoginNetShoes, TBCLIENTE.SenhaNetShoes, TBCLIENTE.IdSellerNetShoes  " & _
        " FROM  " & _
        "   TB_PRODUTOS (NOLOCK) " & _ 
        "   INNER JOIN TBCLIENTE (NOLOCK) ON TB_PRODUTOS.CDCLIENTE = TBCLIENTE.CDCLIENTE " & _ 
        " WHERE  1=1 " & _
        "   AND TB_PRODUTOS.BoolMktPlace = 1    " & _
        "   AND TB_PRODUTOS.CDCLIENTE = "& Chave & _ 
        " ORDER BY " & _
            " TB_PRODUTOS.ID" 
Set Rs = oConn.execute(sSql)
   
if(not Rs.eof) then
	' =======================================
	Response.write "** Recuperou Produtos...<br>"
    response.write "<hr>"
	Response.flush
	' =======================================
     
	do while not Rs.eof
        LoginERP        = trim(rs("LoginNetShoes"))
        SenhaERP        = trim(rs("SenhaNetShoes"))
        IdSellerNetShoes= trim(rs("IdSellerNetShoes"))
        sku             = trim(rs("ID"))
        Qte_Estoque     = trim(rs("Qte_Estoque"))
        UrlApiProduto0  = "http://api-marketplace.netshoes.com.br/api/v2/products/" & sku & "/stocks"

        sConteudo = "{""available"": " & Qte_Estoque & "}"

response.write UrlApiProduto0  & " - " & sConteudo  & "<br>"

        if(sConteudo <> "") then     
            Set http = Server.CreateObject("WinHttp.WinHttpRequest.5.1")
            With http
                .Open "PUT", UrlApiProduto0, false
                .SetRequestHeader "Accept",         "application/json"
                .SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
                .SetRequestHeader "client_id",      LoginERP
                .SetRequestHeader "access_token",   SenhaERP     
                .SetRequestHeader "idseller", IdSellerNetShoes
                .Send(dataImput)
            End With

response.write http.Status & "<br>"
response.write http.responseText & "<br>"
response.write http.StatusText & "<br>"
'response.end 
 
            If http.Status = "204" Then
                Response.Write("<p>Produto atualizado: "& sConteudo &" - " & http.Status & " " & http.StatusText & "</p>")
                response.Flush
            Else
                Response.Write("<br><font color=red>Erro: "& sConteudo &" - " & http.Status & " " & http.StatusText & "</font>")
                response.Flush
            End If

            sConteudo = ""

response.write "<hr>"

            call MyDelay(2)
        end if

	Rs.movenext
	loop

    'if(sConteudo <> "") then sConteudo = left(trim(sConteudo), len(trim(sConteudo))-1)
else
	' =======================================
	Response.write "** sem produtos..<br>"
	Response.flush
	' =======================================
end if
Set Rs = nothing
call Fecha_Conexao()


' ================================================================================
Function DeParaErro( strText ) 

    '201 - Requisição executada com sucesso	
    '400 - Requisição mal-formada	
    '401 - Erro de autenticação	
    '500 - Erro na API

    Select case strText
        case "201"
            DeParaErro = "201 - Requisição executada com sucesso"
        case "400"
            DeParaErro = "400 - Requisição mal-formada"
        case "401"
            DeParaErro = "401 - Erro de autenticação"
        case "500"
            DeParaErro = "500 - Erro na API"
        case else
            DeParaErro = strText
    end select
end Function


Function Base64Encode(inData)
    'rfc1521
    '2001 Antonin Foller, Motobit Software, http://Motobit.cz

    Const Base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    Dim cOut, sOut, I

    'For each group of 3 bytes

    For I = 1 To Len(inData) Step 3
    Dim nGroup, pOut, sGroup

    'Create one long from this 3 bytes.

    nGroup = &H10000 * Asc(Mid(inData, I, 1)) + _
    &H100 * MyASC(Mid(inData, I + 1, 1)) + MyASC(Mid(inData, I + 2, 1))

    'Oct splits the long To 8 groups with 3 bits

    nGroup = Oct(nGroup)

    'Add leading zeros

    nGroup = String(8 - Len(nGroup), "0") & nGroup

    'Convert To base64

    pOut = Mid(Base64, CLng("&o" & Mid(nGroup, 1, 2)) + 1, 1) + _
    Mid(Base64, CLng("&o" & Mid(nGroup, 3, 2)) + 1, 1) + _
    Mid(Base64, CLng("&o" & Mid(nGroup, 5, 2)) + 1, 1) + _
    Mid(Base64, CLng("&o" & Mid(nGroup, 7, 2)) + 1, 1)

    'Add the part To OutPut string

    sOut = sOut + pOut

    'Add a new line For Each 76 chars In dest (76*3/4 = 57)
    'If (I + 2) Mod 57 = 0 Then sOut = sOut + vbCrLf

    Next

    Select Case Len(inData) Mod 3
        Case 1: '8 bit final
            sOut = Left(sOut, Len(sOut) - 2) + "=="
        Case 2: '16 bit final
            sOut = Left(sOut, Len(sOut) - 1) + "="
    End Select

    Base64Encode = sOut
End Function

Function MyASC(OneChar)
    If OneChar = "" Then MyASC = 0 Else MyASC = Asc(OneChar)
End Function


Function Base64Decode(ByVal base64String)
    'rfc1521
    '1999 Antonin Foller, Motobit Software, http://Motobit.cz

    Const Base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    Dim dataLength, sOut, groupBegin

    'remove white spaces, If any

    base64String = Replace(base64String, vbCrLf, "")
    base64String = Replace(base64String, vbTab, "")
    base64String = Replace(base64String, " ", "")

    'The source must consists from groups with Len of 4 chars

    dataLength = Len(base64String)
    If dataLength Mod 4 <> 0 Then
    Err.Raise 1, "Base64Decode", "Bad Base64 string."
        Exit Function
    End If


    ' Now decode each group:

    For groupBegin = 1 To dataLength Step 4
    Dim numDataBytes, CharCounter, thisChar, thisData, nGroup, pOut
    ' Each data group encodes up To 3 actual bytes.

    numDataBytes = 3
    nGroup = 0

    For CharCounter = 0 To 3
    ' Convert each character into 6 bits of data, And add it To
    ' an integer For temporary storage. If a character is a '=', there
    ' is one fewer data byte. (There can only be a maximum of 2 '=' In
    ' the whole string.)

    thisChar = Mid(base64String, groupBegin + CharCounter, 1)

    If thisChar = "=" Then
        numDataBytes = numDataBytes - 1
        thisData = 0
    Else
        thisData = InStr(1, Base64, thisChar, vbBinaryCompare) - 1
    End If
    If thisData = -1 Then
        Err.Raise 2, "Base64Decode", "Bad character In Base64 string."
        Exit Function
    End If

        nGroup = 64 * nGroup + thisData
    Next

    'Hex splits the long To 6 groups with 4 bits

    nGroup = Hex(nGroup)

    'Add leading zeros

    nGroup = String(6 - Len(nGroup), "0") & nGroup

    'Convert the 3 byte hex integer (6 chars) To 3 characters

    pOut = Chr(CByte("&H" & Mid(nGroup, 1, 2))) + _
    Chr(CByte("&H" & Mid(nGroup, 3, 2))) + _
    Chr(CByte("&H" & Mid(nGroup, 5, 2)))

    'add numDataBytes characters To out string

    sOut = sOut & Left(pOut, numDataBytes)
    Next

    Base64Decode = sOut
End Function

' =======================================================================================================
Function RandNo()
    Randomize
    RandNo = Int(99999 * Rnd + 3)
End Function

Sub MyDelay(NumberOfSeconds)
    Dim DateTimeResume
    DateTimeResume= DateAdd("s", NumberOfSeconds, Now())
    Do Until (Now() > DateTimeResume)
    Loop
End Sub

%>