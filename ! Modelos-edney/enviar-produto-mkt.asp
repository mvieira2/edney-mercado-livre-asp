﻿
<%
response.Buffer = true
' response.end
' Server.ScriptTimeout = 9999

dim sConteudoProduto, UrlProdutos, UrlProdutosIntegra, sToken, LoginEmail, sRetorno, sTipoMKT, UrlProdutosWall, sChaveWM, sCdClienteRMKT, IdSellerNetShoes
dim LoginIntegraCommerce, SenhaIntegraCommerce, NomeFantasia, boolRoboWm, sProdutosLote, boolRoboWmZerados, boolRoboWmEstoque, boolRoboWmProdutos, LoginNetShoes, SenhaNetShoes, AccountManagerSkyHub
dim ApiSecretEpicom, ApiKeyEpicom, UrlProdutosEpicom, selprod

'if(session("CdCliente") = "") then response.write "[Erro Loja]": response.End

call Abre_Conexao()

boolRoboWm          = (replace(trim(request("RoboWm")),"'"," ") = "1")
boolRoboWmZerados   = (replace(trim(request("RoboWm")),"'"," ") = "2")
boolRoboWmEstoque   = (replace(trim(request("RoboWm")),"'"," ") = "3")
sTipoMKT            = replace(trim(request("tipo")),"'"," ")
sCdClienteRMKT      = replace(trim(request("int")),"'"," ")
selprod             = replace(trim(request("selprod")),"'"," ")
sProdutosLote       = replace(trim(request("produtos")),"'"," ")
boolRoboWmProdutos  = (sProdutosLote <> "")

UrlProdutos         = "https://api.skyhub.com.br/products"
UrlProdutosAux      = "https://in.skyhub.com.br/products"
UrlProdutosWall     = "https://roboapi.ominic.com.br/api/tarefa/novoProduto"
UrlProdutosIntegra  = "https://in.integracommerce.com.br/api/"
UrlProdutosEpicom   = "https://mhubapi.epicom.com.br/v1"        ' https://sandboxmhubapi.epicom.com.br/v1

if(sCdClienteRMKT = "") then sCdClienteRMKT = session("CdCliente")


' =================================================================================================
sSQlTBCLIENTE = "Select TOP 1 * from TBCLIENTE (nolock) WHERE CdCliente = " & sCdClienteRMKT
set RsTBCLIENTE = oConn.execute(sSQlTBCLIENTE)
if(not RsTBCLIENTE.eof) then
    NomeFantasia         = trim(RsTBCLIENTE("Fantasia"))
    LoginEmail           = trim(RsTBCLIENTE("LoginCobreBem"))
    sToken               = trim(RsTBCLIENTE("SenhaCobreBem"))
    sChaveWM             = trim(RsTBCLIENTE("ChaveMKT")) 
    LoginIntegraCommerce = trim(RsTBCLIENTE("LoginIntegraCommerce"))
    SenhaIntegraCommerce = trim(RsTBCLIENTE("SenhaIntegraCommerce"))
    LoginNetShoes        = trim(RsTBCLIENTE("LoginNetShoes"))
    SenhaNetShoes        = trim(RsTBCLIENTE("SenhaNetShoes"))
    IdSellerNetShoes     = trim(RsTBCLIENTE("IdSellerNetShoes"))
    AccountManagerSkyHub = trim(RsTBCLIENTE("AccountManagerSkyHub"))
    ApiSecretEpicom      = trim(RsTBCLIENTE("ApiSecretEpicom"))
    ApiKeyEpicom         = trim(RsTBCLIENTE("ApiKeyEpicom"))
end if
set RsTBCLIENTE = nothing
' =================================================================================================

if(boolRoboWmZerados OR boolRoboWmEstoque) then 
    sSql = " SELECT top 4000 " 
else
    sSql = " SELECT " 
end if

    
' /// LISTANDO AS CATEGPROAS ======================================================================
sSql = sSql & " TB_PRODUTOS.*, " & _
                "    TB_CATEGORIAS.id AS IDCAT, TB_CATEGORIAS.Categoria, TB_SUBCATEGORIA.SubCategoria " & _
                " FROM  " & _
                "   TB_PRODUTOS (NOLOCK) " & _ 
                "   INNER JOIN TB_CATEGORIAS (NOLOCK) ON TB_PRODUTOS.ID_CATEGORIA = TB_CATEGORIAS.ID " & _ 
                "   INNER JOIN TB_SUBCATEGORIA (NOLOCK) ON TB_PRODUTOS.ID_SUBCATEGORIA = TB_SUBCATEGORIA.ID "

    if(sTipoMKT = "net") then 
        sSql = sSql & " INNER JOIN TB_MARKETPLACES_ATRIBUTOS ON TB_PRODUTOS.ID = TB_MARKETPLACES_ATRIBUTOS.ID_PRODUTO  "
    end if

    sSql = sSql & " WHERE 1=1 " & _
                "   AND TB_PRODUTOS.CDCLIENTE       = "& sCdClienteRMKT & _ 
                "   AND TB_CATEGORIAS.CDCLIENTE     = "& sCdClienteRMKT & _ 
                "   /*AND TB_PRODUTOS.BoolMktPlace    = 1*/  "

    if(boolRoboWm) then 
        sSql = sSql & " AND ISNULL(TB_PRODUTOS.CampoAux1, '') <> ''  " 
    end if

    if(boolRoboWmZerados) then 
        sSql = sSql & " AND TB_PRODUTOS.Qte_Estoque = 0 " 
        sSql = sSql & " AND ISNULL(TB_PRODUTOS.CampoAux21, '') = ''  " 
    end if

    if(boolRoboWmEstoque) then 
        sSql = sSql & " AND TB_PRODUTOS.Qte_Estoque > 0 " 
        sSql = sSql & " AND ISNULL(TB_PRODUTOS.CampoAux19, '') = ''  " 
    end if

    if(sProdutosLote <> "") then 
        if(sCdClienteRMKT <> "228") then 
            sSql = sSql & " AND TB_PRODUTOS.id in ("& sProdutosLote &")  " 
        else
            sSql = sSql & " AND TB_PRODUTOS.NumeroSerie in ('"& replace(sProdutosLote, ",", "','") &"')  " 
        end if
    end if

    if(selprod <> "") then 
        if(Right(trim(selprod), 1) = ",") then 
            selprod = left(trim(selprod), len(trim(selprod))-1)
        end if

        sSql = sSql & " AND TB_PRODUTOS.id in ("& selprod &")  " 
    end if
    
    sSql = sSql & " ORDER BY TB_PRODUTOS.ID " 

    '"   AND TB_PRODUTOS.ID = 656612 " & _

response.write sSql & "<br>"
response.write "<hr><br><br>"
response.Flush
'response.end 
Set Rs = oConn.execute(sSql)

sConteudoProduto = ""

if(not rs.eof) then

	do while not rs.eof
        Id                          = trim(rs("Id"))
        Id_Categoria                = trim(rs("Id_Categoria"))
        Categoria                   = trim(rs("Categoria"))
        SubCategoria                = trim(rs("SubCategoria"))
        IdSubCategoria              = trim(rs("Id_SubCategoria"))
        CodReferencia               = trim(rs("CodReferencia"))
        Descricao                   = replace(replace(trim(rs("Descricao")),"""",""),"'","")
        DescricaoJP                 = trim(rs("Descricao_JP"))
        DescricaoC                  = replace(replace(trim(rs("DescricaoBreve")),"""",""),"'","")
        Destaque                    = trim(rs("Destaque"))
        Peso                        = trim(rs("Peso"))
        Preco_Consumidor            = trim(rs("Preco_Consumidor"))
        Preco_APrazo                = trim(rs("Preco_APrazo"))
        Preco_MKT                   = trim(rs("Preco_MKT"))
        NumeroSerie                 = trim(rs("NumeroSerie"))
        Qte_Estoque                 = trim(rs("Qte_Estoque"))
        Qte_EstoqueMKT              = trim(rs("Qte_Estoque_MKT"))
        Qte_Parcelamento            = trim(rs("Qte_Parcelamento"))
        Promocao                    = trim(rs("Promocao"))
        Lancamento                  = trim(rs("Lancamento"))
        Imagem                      = trim(rs("Imagem"))
        Imagem2                     = trim(rs("Imagem2"))
        Imagem3                     = trim(rs("Imagem3"))
        Imagem4                     = trim(rs("Imagem4"))
        DescontoTipo                = trim(rs("DescontoTipo"))
        DescontoValor               = trim(rs("DescontoValor"))
        DataIniLan                  = trim(rs("DataIniLan"))
        DataFimLan                  = trim(rs("DataFimLan"))
        DataIniPromo                = trim(rs("DataIniPromo"))
        DataFimPromo                = trim(rs("DataFimPromo"))
        fabricante                  = trim(rs("fabricante"))
        Largura                     = trim(rs("Largura"))
        Altura                      = trim(rs("Altura"))
        Length                      = trim(rs("comprimento"))
        CampoAux1                   = trim(rs("CampoAux1"))
        Ativo                       = trim(rs("Ativo"))
        BoolMktPlace                = trim(rs("BoolMktPlace"))
        CodigoBarras                = trim(rs("CodigoBarras"))
        RetornoStatusNetShoes       = trim(rs("RetornoStatusNetShoes"))
        Preco_Consumidor_Promocao   = 0

        if(fabricante = "" or isnull(fabricante)) then fabricante = NomeFantasia

		if(Promocao = "1")  then 
			BoolDataPromocao = ( (cdate(trim(Rs("DataIniPromo"))) <= Date) and (cDate(trim(Rs("DataFimPromo"))) >= Date) )
		end if

        DescricaoC = Trata_Palavra(DescricaoC)

        if(DescricaoC = "&nbsp;" or DescricaoC = "" or isnull(DescricaoC) or len(DescricaoC) < 3) then DescricaoC = "Decrição do Produto"

        if(DescricaoJP <> "" and not isnull(DescricaoJP)) then Descricao = DescricaoJP

        if(sTipoMKT <> "cen") then 
            if(Imagem <> "" and not isnull(Imagem))     then Imagem  =  "https://imagensproduto.ominic.com.br/EcommerceNew/upload/produto/" & Imagem
            if(Imagem2 <> "" and not isnull(Imagem2))   then Imagem2 =  ",""https://imagensproduto.ominic.com.br/EcommerceNew/upload/produto/" & Imagem2 & """"
            if(Imagem3 <> "" and not isnull(Imagem3))   then Imagem3 =  ",""https://imagensproduto.ominic.com.br/EcommerceNew/upload/produto/" & Imagem3 & """"
            if(Imagem4 <> "" and not isnull(Imagem4))   then Imagem4 =  ",""https://imagensproduto.ominic.com.br/EcommerceNew/upload/produto/" & Imagem4 & """"
        end if

        if(Preco_MKT <> "" and not isnull(Preco_MKT)) then 
            if(cdbl(Preco_MKT) > 0) then 
                if(BoolDataPromocao) then 
                    if(DescontoTipo = "R") then 
                        Preco_Consumidor            = Preco_MKT
                        Preco_Consumidor_Promocao   = formatNumber((cdbl(Preco_MKT) - cdbl(DescontoValor) ),2)   
                    end if  
                else
                    Preco_Consumidor            = Preco_MKT
                    Preco_Consumidor_Promocao   = Preco_MKT
                end if
            end if
        else
            if(BoolDataPromocao) then 
                if(DescontoTipo = "R") then 
                    Preco_Consumidor_Promocao = formatNumber((cdbl(Preco_Consumidor) - cdbl(DescontoValor) ),2)   
                end if  
            else
                Preco_Consumidor_Promocao = Preco_Consumidor
            end if
        end if

        if(Qte_EstoqueMKT <> "" and not isnull(Qte_EstoqueMKT)) then 
            if(cdbl(Qte_EstoqueMKT) > 0) then 
                Qte_Estoque = Qte_EstoqueMKT
            end if
        end if

        if(Ativo = "1") then     
            Ativo = "enabled"
        else
            Ativo = "disabled"
        end if

        if(BoolMktPlace = "1") then     
            BoolMktPlace = "enabled"
            disponivel   = "true"   
        else
            BoolMktPlace = "disabled"
            disponivel   = "false"
        end if

        if(Length <> "" and IsNumeric(Length)) then Length  = replace(replace(formatNumber((Length),2),".",""),",","."): else Length = "0.2" : end if
        if(Length = "0" OR Length = "0.00") then Length = "0.2"

        Sku = Id    
        if((CodReferencia <> "" and not isnull(CodReferencia)) and sCdClienteRMKT = "246") then Sku = CodReferencia

        if(CodigoBarras = "" or isnull(CodigoBarras)) then CodigoBarras = NumeroSerie

        sConteudoProduto = "{""product"":{""sku"":"""& Sku &""", ""name"":"""& replace(trim(Descricao),"#","''") &""",""description"":"""& DescricaoC &""",""status"":"""& BoolMktPlace &""",""qty"":"& Qte_Estoque &",""price"":"& replace(replace(formatNumber(Preco_Consumidor,2),".",""),",",".") &",""promotional_price"":"& replace(replace(Preco_Consumidor_Promocao,".",""),",",".") &",""cost"":"& replace(replace(formatNumber(Preco_Consumidor,2),".",""),",",".") &",""weight"":"& replace(replace(Peso,".",""),",",".") &",""height"":"& replace(replace(Altura,".",""),",",".") &",""length"":"& length &",""width"":"& replace(replace(Largura,".",""),",",".") &",""brand"":"""& fabricante &""",""ean"":"""& CodigoBarras &""",""categories"":[{""code"":"""& Id_Categoria &""",""name"":"""& Categoria &"""}],""images"":["""&  Imagem &""""& Imagem2 & Imagem3 & Imagem4 & "]"
        sConteudoProduto = sConteudoProduto & "}}"


        if(sConteudoProduto <> "") then                   
            ProdutoEnviar = ""

            Select case sTipoMKT

' **************************************************************************************************************
                case "net"   ' NETSHOES
' **************************************************************************************************************

                    Imagem              = trim(rs("Imagem"))
                    Imagem2             = trim(rs("Imagem2"))
                    Imagem3             = trim(rs("Imagem3"))
                    Imagem4             = trim(rs("Imagem4"))
                    if(Imagem <> "" and not isnull(Imagem))     then Imagem  =  "{""url"":""https://imagensproduto.ominic.com.br/EcommerceNew/upload/produto/" & Imagem &"""}"
                    if(Imagem2 <> "" and not isnull(Imagem2))   then Imagem2 =  ",{""url"":""https://imagensproduto.ominic.com.br/EcommerceNew/upload/produto/" & Imagem2 &"""}"
                    if(Imagem3 <> "" and not isnull(Imagem3))   then Imagem3 =  ",{""url"":""https://imagensproduto.ominic.com.br/EcommerceNew/upload/produto/" & Imagem3 &"""}"
                    if(Imagem4 <> "" and not isnull(Imagem4))   then Imagem4 =  ",{""url"":""https://imagensproduto.ominic.com.br/EcommerceNew/upload/produto/" & Imagem4 &"""}"

                    UrlProdutosIntegra  = "http://api-marketplace.netshoes.com.br/api/v2"       ' "http://api-sandbox.netshoes.com.br/api/v2"

                    if(Preco_Consumidor <> "" and not isnull(Preco_Consumidor)) then                    Preco_Consumidor            = replace(replace(formatNumber(Preco_Consumidor,2),".",""),",",".")
                    if(Preco_Consumidor_Promocao <> "" and not isnull(Preco_Consumidor_Promocao)) then  Preco_Consumidor_Promocao   = replace(replace(formatNumber(Preco_Consumidor_Promocao,2),".",""),",",".")
                    if(Preco_Consumidor_Promocao = 0) then                                              Preco_Consumidor_Promocao = Preco_Consumidor
                    if(Largura <> "" and not isnull(Largura)) then                                      Largura                     = replace(replace(Largura,".",""),",",".")
                    if(Largura <> "" and IsNumeric(Largura)) then                                       Largura                     = replace(replace(formatNumber((Largura / 100),2),".",""),",",".")
                    if(Altura <> "" and not isnull(Altura)) then                                        Altura                      = replace(replace(Altura,".",""),",",".")
                    if(Altura <> "" and IsNumeric(Altura)) then                                         Altura                      = replace(replace(formatNumber((Altura / 100),2),".",""),",",".")
                    if(Length <> "" and IsNumeric(Length)) then                                         Length                      = replace(replace(formatNumber((Length / 100),2),".",""),",","."): else Length = "0.2" : end if
                    if(Length = "0" OR Length = "0.00") then                                            Length                      = "0.2"
                    if(Peso <> "" and not isnull(Peso)) then                                            Peso                        = replace(replace(formatNumber(Peso,2),".",""),",",".")

		            ' VERIFICAR CAMPOS MARKTEPLACES ---------------------------------------
		            sSqlMKT = "SELECT TOP 1 * FROM TB_MARKETPLACES_ATRIBUTOS (NOLOCK) WHERE CDCLIENTE = "& sCdClienteRMKT &" AND id_produto = "& Id
		            Set RsMKT = oConn.execute(sSqlMKT)
			
                    NetShoesMarca           = ""
                    NetShoesCor             = ""
                    NetShoesSabor           = ""
                    NetShoesTamanho         = ""
                    NetShoesCategoria       = ""
                    NetShoesSubCategoria    = ""
                    Department              = ""
                    NetShoesPreco           = "0"
                    NetShoesPrecoDE         = "0"
                    NetShoesEstoque         = ""

		            if(not RsMKT.eof) then
                        if(RsMKT("Marca") <> "" and not isnull(RsMKT("Marca")))                 then fabricante             = trim(RsMKT("Marca"))
                        if(RsMKT("Cor") <> "" and not isnull(RsMKT("Cor")))                     then NetShoesCor            = trim(RsMKT("Cor"))
                        if(RsMKT("Sabor") <> "" and not isnull(RsMKT("Sabor")))                 then NetShoesSabor          = trim(RsMKT("Sabor"))
                        if(RsMKT("Tamanho") <> "" and not isnull(RsMKT("Tamanho")))             then NetShoesTamanho        = trim(RsMKT("Tamanho"))
                        if(RsMKT("Department") <> "" and not isnull(RsMKT("Department")))       then department             = trim(RsMKT("Department"))
                        if(RsMKT("Categoria") <> "" and not isnull(RsMKT("Categoria")))         then Categoria              = trim(RsMKT("Categoria"))
                        if(RsMKT("SubCategoria") <> "" and not isnull(RsMKT("SubCategoria")))   then SubCategoria           = trim(RsMKT("SubCategoria"))
                        if(RsMKT("preco") <> "" and not isnull(RsMKT("preco")))                 then NetShoesPreco          = trim(RsMKT("preco"))
                        if(RsMKT("precoDE") <> "" and not isnull(RsMKT("precoDE")))             then NetShoesPrecoDE        = trim(RsMKT("precoDE"))
                        if(RsMKT("estoque") <> "" and not isnull(RsMKT("estoque")))             then NetShoesEstoque        = trim(RsMKT("estoque"))
                    end if
                    Set RsMKT = nothing
                    ' VERIFICAR CAMPOS MARKTEPLACES ---------------------------------------


response.Write "URL: "& (UrlProdutosIntegra & "/products") &"<br>"
response.Write "LoginNetShoes:"&    LoginNetShoes & "<br>"
response.Write "SenhaNetShoes:"&    SenhaNetShoes & "<br>"
response.Write "IdSellerNetShoes:"& IdSellerNetShoes & "<br><br>"
response.Flush


' CRIAR PRODUTO =======================================================================
                    sConteudoProdutoIntegra = "{""sku"":"""& Id &""",""productGroup"":"""& Categoria &""",""department"":"""& department &""",""productType"":"""& SubCategoria &""",""brand"": """& fabricante &""",""name"":"""&  replace(trim(Descricao),"#","''") &""",""description"":"""&  DescricaoC &""",""color"":"""&  NetShoesCor &""",""gender"": ""Unissex"",""manufacturerCode"":"""",""size"":"""& NetShoesTamanho &""",""eanIsbn"": """& CodigoBarras &""",""height"":"& Altura &",""width"":"& Largura & _ 
                                                ",""depth"":"& Length &",""weight"":"& Peso &",""video"":"""",""images"":["& Imagem & Imagem2 & Imagem3 & Imagem4 & "],""attributes"":["


                    boolTrechoNetShoesClassificacao = (Id = "740240" OR Id = "740241" OR Id = "740242" OR Id = "740246" OR Id = "740247" OR Id = "740248" OR Id = "740248" OR Id = "740249" OR Id = "740262" OR Id = "740263")


                    sConteudoProdutoIntegra2 = ""
                    if(boolTrechoNetShoesClassificacao) then 
                        sConteudoProdutoIntegra2 = sConteudoProdutoIntegra2 & "{""name"":""Classificação"",""values"":[""Residencial""]},"
                    end if

                    if(sConteudoProdutoIntegra2 <> "") then 
                        if(Right(trim(sConteudoProdutoIntegra2),1) = "," ) then 
                            sConteudoProdutoIntegra2 = left(trim(sConteudoProdutoIntegra2), len(trim(sConteudoProdutoIntegra2))-1)
                        end if
                        sConteudoProdutoIntegra = (sConteudoProdutoIntegra & sConteudoProdutoIntegra2 & "]}")
                    else
                        sConteudoProdutoIntegra = sConteudoProdutoIntegra & "]}"
                    end if

                    Set oHttpRequest = Server.CreateObject("Microsoft.XMLHTTP")    
                    with oHttpRequest 
                        .Open "POST", (UrlProdutosIntegra & "/products"), false, prLoginV, prSenhaV
                        .SetRequestHeader "Accept",   "application/json"
                        .SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
                        .SetRequestHeader "client_id", LoginNetShoes
                        .SetRequestHeader "access_token", SenhaNetShoes
                        .SetRequestHeader "idseller", IdSellerNetShoes
                        .Send sConteudoProdutoIntegra
                        sRetorno        = .status
                        sRetornoErro    = .responseText
                    End With 
                    Set oHttpRequest = Nothing

                    sConteudoProdutoIntegraRept = sConteudoProdutoIntegra

                    call MyDelay(2)

response.Write "Metodo: POST<br>"
response.Write "sConteudoProdutoIntegra:    "&  sConteudoProdutoIntegra & "<br>"
response.Write "sRetorno:                   "&  sRetorno & "<br>"
response.Write "sRetornoErro(1):            "&  sRetornoErro & "<br><br>"
response.Flush
'   CRIAR PRODUTO =======================================================================

                    if(sRetorno = "409") then 
                        ' PEDIDO JA EXISTE ==================================================================
                        Set oHttpRequestPut = Server.CreateObject("MSXML2.ServerXMLHTTP")
                        with oHttpRequestPut 
                            .Open "PUT", (UrlProdutosIntegra & "/products/"& Id), false, prLoginV, prSenhaV
                            .SetRequestHeader "Accept",   "application/json"
                            .SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
                            .SetRequestHeader "client_id", LoginNetShoes
                            .SetRequestHeader "access_token", SenhaNetShoes
                            .SetRequestHeader "idseller", IdSellerNetShoes
                            .Send sConteudoProdutoIntegraRept
                            sRetorno        = .status
                            sRetornoErro    = .responseText
                        End With 
                        Set oHttpRequestPut = Nothing

                        response.Write "Metodo: PUT<br>"
                        response.Write "<p>URL:                         "&  (UrlProdutosIntegra & "/products/"& Id) &"<br>"
                        response.Write "sConteudoProdutoIntegraRept:    "&  sConteudoProdutoIntegraRept & "<br>"
                        response.Write "sRetorno:"&                         sRetorno & "<br>"
                        response.Write "sRetornoErro(4):"&                  sRetornoErro & "<br><br>"
                        response.Flush
                    end if

                    sSql = "update TB_PRODUTOS set RetornoStatusNetShoes = '"& sRetorno &"', RetornoErroNetShoes = '"& sRetornoErro &"', RetornoRetornoNetShoes = '"& sRetornoErro &"' where id = " & Id
                    oConn.execute(sSql)

                    if(NetShoesPreco <> "" and NetShoesPreco <> "0") then Preco_Consumidor_Promocao = NetShoesPreco
                    if(NetShoesPrecoDE = "" or NetShoesPrecoDE = "0") then NetShoesPrecoDE = Preco_Consumidor_Promocao
    
                    '   CRIAR PREÇO =======================================================================
                    call InserirAtualizarPricesNetShoes(Id, NetShoesPrecoDE, Preco_Consumidor_Promocao)

                    call MyDelay(2)
    
                    '   CRIAR ESTOQUE =======================================================================
                    call InserirAtualizarStocksNetShoes(Id, Qte_Estoque, NetShoesEstoque)


' NETSHOES =============================================================================



            end select

            if(sTipoMKT <> "in" and sTipoMKT <> "wm" and sTipoMKT <> "net" and sTipoMKT <> "cen") then 
                response.Write "<p>URL:             " & ProdutoEnviar & "<br>"
                response.Write "chave:              "& sChaveWM & "<br>"
                'response.Write "X-User-Email:       "&  LoginEmail &"<br>"
                'response.Write "X-User-Token:       "& sToken & "<br><br>"
                response.Write "sConteudoProduto:    "& sRetornoText & "<br><br>"
                response.Write "sRetorno:"&          sRetorno & "</p><br>"
                response.Flush
            end if

            sSql = "INSERT INTO TB_HISTORICO_EXPORTACAO_SKYHUB (Cdcliente, Id_Produto, Erro) VALUES ('"& sCdClienteRMKT &"', '"& Id &"', '"& DeParaErro(sRetorno) &"') "
            oConn.execute(sSql)

            sConteudoProduto = ""

            response.Write "<hr><br>"
            response.Flush
        end if

    rs.movenext
	loop
else
    response.write "[sem produtos]"
end if
Set Rs = nothing
call Fecha_Conexao()


' ================================================================
Sub InserirAtualizarStocksNetShoes(prId, prQte_Estoque, prNetShoesEstoque)

    dim sConteudoProdutoIntegraSub, sRetornoSub, sRetornoErroSub, EstoqueAlterar
    
    if(prNetShoesEstoque <> "" and not isnull(prNetShoesEstoque) and IsNumeric(prNetShoesEstoque) ) then 
        EstoqueAlterar = prNetShoesEstoque
    else
        EstoqueAlterar = prQte_Estoque
    end if

    sConteudoProdutoIntegraSub = "{""available"": "& EstoqueAlterar &"}"

    Set oHttpRequest = Server.CreateObject("Microsoft.XMLHTTP")    
    with oHttpRequest 
        .Open "POST", (UrlProdutosIntegra & "/products/"& prId &"/stocks"), false, prLoginV, prSenhaV
        .SetRequestHeader "Accept",   "application/json"
        .SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
        .SetRequestHeader "client_id", LoginNetShoes
        .SetRequestHeader "access_token", SenhaNetShoes
        .SetRequestHeader "idseller", IdSellerNetShoes
        .Send sConteudoProdutoIntegraSub
        sRetornoSub        = .status
        sRetornoErroSub    = .responseText
    End With 
    Set oHttpRequest = Nothing
   
    if(sRetornoSub = "409") then 
        Set oHttpRequestPut = Server.CreateObject("MSXML2.ServerXMLHTTP")
        with oHttpRequestPut 
            .Open "PUT", (UrlProdutosIntegra & "/products/"& prId &"/stocks"), false, prLoginV, prSenhaV
            .SetRequestHeader "Accept",   "application/json"
            .SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
            .SetRequestHeader "client_id", LoginNetShoes
            .SetRequestHeader "access_token", SenhaNetShoes
            .SetRequestHeader "idseller", IdSellerNetShoes
            .Send sConteudoProdutoIntegraSub
            sRetornoSub        = .status
            sRetornoErroSub    = .responseText
        End With 
        Set oHttpRequestPut = Nothing     
    end if

    response.Write "<p>URL:                     "& (UrlProdutosIntegra & "/products/"& prId &"/stocks") &"<br>"
    response.Write "sConteudoProdutoIntegra:    "& sConteudoProdutoIntegraSub & "<br>"
    response.Write "sRetorno:                   "&  sRetornoSub & "<br>"
    response.Write "sRetornoErro(2):            "&  sRetornoErroSub & "<br><br>"
    response.Flush
End Sub


' ================================================================
Sub InserirAtualizarPricesNetShoes(prId, prPreco_Consumidor, prPreco_Consumidor_Promocao)
    dim sConteudoProdutoIntegraSub, sRetornoSub, sRetornoErroSub
    
    sConteudoProdutoIntegraSub = "{""listPrice"": "& replace(prPreco_Consumidor, ",", ".") &", ""salePrice"": "& replace(prPreco_Consumidor_Promocao, ",", ".") &"}"

    Set oHttpRequest = Server.CreateObject("Microsoft.XMLHTTP")    
    with oHttpRequest 
        .Open "POST", (UrlProdutosIntegra & "/products/"& prId &"/prices"), false, prLoginV, prSenhaV
        .SetRequestHeader "Accept",   "application/json"
        .SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
        .SetRequestHeader "client_id", LoginNetShoes
        .SetRequestHeader "access_token", SenhaNetShoes
        .SetRequestHeader "idseller", IdSellerNetShoes
        .Send sConteudoProdutoIntegraSub
        sRetornoSub        = .status
        sRetornoErroSub    = .responseText
    End With 
    Set oHttpRequest = Nothing
   
    if(sRetornoSub = "409") then 
        Set oHttpRequestPut = Server.CreateObject("MSXML2.ServerXMLHTTP")
        with oHttpRequestPut 
            .Open "PUT", (UrlProdutosIntegra & "/products/"& prId &"/prices"), false, prLoginV, prSenhaV
            .SetRequestHeader "Accept",   "application/json"
            .SetRequestHeader "Content-Type",   "application/json; charset=utf-8"
            .SetRequestHeader "client_id", LoginNetShoes
            .SetRequestHeader "access_token", SenhaNetShoes
            .SetRequestHeader "idseller", IdSellerNetShoes
            .Send sConteudoProdutoIntegraSub
            sRetornoSub        = .status
            sRetornoErroSub    = .responseText
        End With 
        Set oHttpRequestPut = Nothing     
    end if

    response.Write "<p>URL:                     "& (UrlProdutosIntegra & "/products/"& prId &"/prices") &"<br>"
    response.Write "sConteudoProdutoIntegra:    "& sConteudoProdutoIntegraSub & "<br>"
    response.Write "sRetorno:                   "&  sRetornoSub & "<br>"
    response.Write "sRetornoErro(2):            "&  sRetornoErroSub & "<br><br>"
    response.Flush
End Sub



' ================================================================
Function Trata_Palavra(Palavra)
	Dim Letra, sPalavraC
	
	Trata_Palavra	= ""
	sPalavraC		= ""

	if(Palavra = "") then exit function
	
	Palavra = replace(replace(replace(replace(replace(replace(replace(replace(replace(Palavra, "<BR>",""), "<br>",""), chr(160),""), """",""), "'",""), "''",""), chr(146),""), chr(40),""), chr(41),"")
	
'response.write Palavra & "<br>"

    if(false) then 
	    for i = 1 to len(Palavra)
		    Letra = mid(LCase(Palavra), i, 1)

    'response.write Letra & " - " & asc(Letra) & "<br>"

		    Select Case lcase(Letra)
			    Case "á","à","ã","â","â"
				    Letra = "a"
			    Case "é","ê","è"
				    Letra = "e"
			    Case "í","ì","í"
				    Letra = "i"
			    Case "ó","ô","õ","ò"
				    Letra = "o"
			    Case "ú","ù"
				    Letra = "u"
			    Case "ç"
				    Letra = "c"
			    Case "£"
				    Letra = " "
			    Case "ª"
				    Letra = " "
			    Case "–"
				    Letra = " "
			    Case "&"
				    Letra = " "
			    Case "%"
				    Letra = " "
			    Case "*"
				    Letra = " "
			    Case "º"
				    Letra = " "
			    Case "'"
				    Letra = " "
			    Case "<"
				    Letra = " "
			    Case ">"
				    Letra = " "
			    Case """"
				    Letra = " "
			    Case ";"
				    Letra = " "
		    End Select

		    sPalavraC = (sPalavraC & ucase(Letra))
	    next
        Trata_Palavra = sPalavraC
    else
	    Trata_Palavra = Palavra
    end if
End Function


' ================================================================================
Function DeParaErro( strText ) 

    '201 - Requisição executada com sucesso	
    '400 - Requisição mal-formada	
    '401 - Erro de autenticação	
    '500 - Erro na API

    Select case strText
        case "201"
            DeParaErro = "201 - Requisição executada com sucesso"
        case "400"
            DeParaErro = "400 - Requisição mal-formada"
        case "401"
            DeParaErro = "401 - Erro de autenticação"
        case "500"
            DeParaErro = "500 - Erro na API"
        case else
            DeParaErro = strText
    end select
end Function



' ================================================================================
FUNCTION stripHTML(strHTML)
  Dim objRegExp, strOutput, tempStr
  Set objRegExp = New Regexp
  objRegExp.IgnoreCase = True
  objRegExp.Global = True
  objRegExp.Pattern = "<(.|n)+?>"
  'Replace all HTML tag matches with the empty string
  strOutput = objRegExp.Replace(strHTML, "")
  'Replace all < and > with &lt; and &gt;
  strOutput = Replace(strOutput, "<", "&lt;")
  strOutput = Replace(strOutput, ">", "&gt;")
  stripHTML = strOutput    'Return the value of strOutput
  Set objRegExp = Nothing
END FUNCTION


' ================================================================================
Function RemoveHTML( strText ) 
    Dim nPos1
    Dim nPos2
    
    nPos1 = InStr(strText, "<") 
    Do While nPos1 > 0 
        nPos2 = InStr(nPos1 + 1, strText, ">") 
        If nPos2 > 0 Then 
            strText = Left(strText, nPos1 - 1) & Mid(strText, nPos2 + 1) 
        Else 
            Exit Do 
        End If 
        nPos1 = InStr(strText, "<") 
    Loop 
    
    RemoveHTML = replace(replace(replace(strText, vbcrlf, "-"), """", ""),"'", "")
End Function 


' =========================================================================
Sub MyDelay(NumberOfSeconds)
    Dim DateTimeResume
    DateTimeResume= DateAdd("s", NumberOfSeconds, Now())
    Do Until (Now() > DateTimeResume)
    Loop
End Sub


Function Base64Encode(inData)
    'rfc1521
    '2001 Antonin Foller, Motobit Software, http://Motobit.cz

    Const Base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    Dim cOut, sOut, I

    'For each group of 3 bytes

    For I = 1 To Len(inData) Step 3
    Dim nGroup, pOut, sGroup

    'Create one long from this 3 bytes.

    nGroup = &H10000 * Asc(Mid(inData, I, 1)) + _
    &H100 * MyASC(Mid(inData, I + 1, 1)) + MyASC(Mid(inData, I + 2, 1))

    'Oct splits the long To 8 groups with 3 bits

    nGroup = Oct(nGroup)

    'Add leading zeros

    nGroup = String(8 - Len(nGroup), "0") & nGroup

    'Convert To base64

    pOut = Mid(Base64, CLng("&o" & Mid(nGroup, 1, 2)) + 1, 1) + _
    Mid(Base64, CLng("&o" & Mid(nGroup, 3, 2)) + 1, 1) + _
    Mid(Base64, CLng("&o" & Mid(nGroup, 5, 2)) + 1, 1) + _
    Mid(Base64, CLng("&o" & Mid(nGroup, 7, 2)) + 1, 1)

    'Add the part To OutPut string

    sOut = sOut + pOut

    'Add a new line For Each 76 chars In dest (76*3/4 = 57)
    'If (I + 2) Mod 57 = 0 Then sOut = sOut + vbCrLf

    Next

    Select Case Len(inData) Mod 3
        Case 1: '8 bit final
            sOut = Left(sOut, Len(sOut) - 2) + "=="
        Case 2: '16 bit final
            sOut = Left(sOut, Len(sOut) - 1) + "="
    End Select

    Base64Encode = sOut
End Function

Function MyASC(OneChar)
    If OneChar = "" Then MyASC = 0 Else MyASC = Asc(OneChar)
End Function


Function Base64Decode(ByVal base64String)
    'rfc1521
    '1999 Antonin Foller, Motobit Software, http://Motobit.cz

    Const Base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    Dim dataLength, sOut, groupBegin

    'remove white spaces, If any

    base64String = Replace(base64String, vbCrLf, "")
    base64String = Replace(base64String, vbTab, "")
    base64String = Replace(base64String, " ", "")

    'The source must consists from groups with Len of 4 chars

    dataLength = Len(base64String)
    If dataLength Mod 4 <> 0 Then
    Err.Raise 1, "Base64Decode", "Bad Base64 string."
        Exit Function
    End If


    ' Now decode each group:

    For groupBegin = 1 To dataLength Step 4
    Dim numDataBytes, CharCounter, thisChar, thisData, nGroup, pOut
    ' Each data group encodes up To 3 actual bytes.

    numDataBytes = 3
    nGroup = 0

    For CharCounter = 0 To 3
    ' Convert each character into 6 bits of data, And add it To
    ' an integer For temporary storage. If a character is a '=', there
    ' is one fewer data byte. (There can only be a maximum of 2 '=' In
    ' the whole string.)

    thisChar = Mid(base64String, groupBegin + CharCounter, 1)

    If thisChar = "=" Then
        numDataBytes = numDataBytes - 1
        thisData = 0
    Else
        thisData = InStr(1, Base64, thisChar, vbBinaryCompare) - 1
    End If
    If thisData = -1 Then
        Err.Raise 2, "Base64Decode", "Bad character In Base64 string."
        Exit Function
    End If

        nGroup = 64 * nGroup + thisData
    Next

    'Hex splits the long To 6 groups with 4 bits

    nGroup = Hex(nGroup)

    'Add leading zeros

    nGroup = String(6 - Len(nGroup), "0") & nGroup

    'Convert the 3 byte hex integer (6 chars) To 3 characters

    pOut = Chr(CByte("&H" & Mid(nGroup, 1, 2))) + _
    Chr(CByte("&H" & Mid(nGroup, 3, 2))) + _
    Chr(CByte("&H" & Mid(nGroup, 5, 2)))

    'add numDataBytes characters To out string

    sOut = sOut & Left(pOut, numDataBytes)
    Next

    Base64Decode = sOut
End Function

%>